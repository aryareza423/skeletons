<?php

namespace App\Models;

use App\Models\Checkout;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Keranjang extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','user_id', 'checkout_id',  'produk_id', 'kuantitas', 'size_id'];
    protected $appends = ['subtotal'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function produk() {
        return $this->belongsTo(Produk::class);
    }
    public function size() {
        return $this->belongsTo(Size::class);
    }

    public function checkout() {
        return $this->belongsTo(Checkout::class);
    }

    public function getSubtotalAttribute() {
        return $this->kuantitas * $this->produk->price;
    }

}
