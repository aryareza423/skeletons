<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','rating', 'komen', 'gambar', 'produk_id', 'user_id'];
    public function produk() {
        return $this->belongsTo(Produk::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
