<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    use HasFactory;
    protected $fillable = ['kuantitas', 'sub_total', 'checkout_id', 'produk_id', 'size'];

    public function checkout() {
        return $this->belongsTo(Checkout::class);
    }

    public function produk() {
        return $this->belongsTo(Produk::class);
    }
    public function sizes() {
        return $this->belongsTo(Size::class, 'size');
    }
}
