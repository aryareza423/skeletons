<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GambarSpesifikasi extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','gambar', 'spesifikasi_produk_id' ];

    public function spesifikasiproduks() {
        return $this->belongsTo(SpesifikasiProduk::class);
    }
}
