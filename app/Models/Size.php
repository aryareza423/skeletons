<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','size', 'stock', 'produk_id' ];

    public function produk() {
        return $this->belongsTo(Produk::class);
    }
 
}
