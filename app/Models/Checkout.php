<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','status', 'bukti_pembayaran', 'total_harga', 'user_id', 'bank_id'];
    protected $with = ['pengiriman', 'pesanans'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function bank() {
        return $this->belongsTo(Bank::class);
    }

    public function pengiriman() {
        return $this->hasOne(Pengiriman::class);
    }

    public function pesanans() {
        return $this->hasMany(Pesanan::class);
    }
}
