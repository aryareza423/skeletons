<?php

namespace App\Models;
use App\Traits\Uuid;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','gambar','nm_bank', 'no_rekening', 'atas_nama'];

}
