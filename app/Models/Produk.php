<?php

namespace App\Models;

use App\Models\Size;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Produk extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','kategori_id', 'nm_produk', 'deskripsi', 'gambar', 'price', 'berat','spesifikasi_produk_id','size_id'];
    protected $appends = ['is_rated'];

    public function kategori() {
        return $this->belongsTo(Kategori::class);
    }

    public function size() {
        return $this->hasMany(Size::class);
    }

    public function spesifikasiproduks() {
        return $this->hasOne(SpesifikasiProduk::class);
    }

    public function pesanan() {
        return $this->hasMany(Pesanan::class);
    }

    public function getIsRatedAttribute() {
        if (!auth()->user()) return false;
        return !!Rating::where('produk_id', $this->id)->where('user_id', auth()->user()->id)->first();
    }

    public function getTerjualAttribute() {
        $terjual = 0;
        foreach ($this->pesanan as $pesanan) {
            $terjual += $pesanan->kuantitas;
        }
        return $terjual;
    }
}
