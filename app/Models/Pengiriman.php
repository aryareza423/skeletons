<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    use HasFactory;
protected $fillable = ['uuid','nama','no_hp','harga_ongkir', 'checkout_id', 'nm_kurir', 'jenis_kurir', 'kota_id', 'provinsi_id','nm_penerima', 'alamat',  'user_id', 'kode_pos', 'bank_id'];
protected $table = 'pengiriman'; 

    public function kota() {
        return $this->belongsTo(Kota::class);
    }

    public function provinsi() {
        return $this->belongsTo(Provinsi::class);
    }

    public function checkout() {
        return $this->belongsTo(Checkout::class);
    }

    public function bank() {
        return $this->belongsTo(Bank::class);
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
