<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpesifikasiProduk extends Model
{
    use HasFactory;

    protected $fillable = ['uuid','produk_id', 'negara_asal', 'model_sepatu', 'merek', 'gambarspesifikasi_id' ];

    public function produk() {
        return $this->belongsTo(Produk::class);
    }
    public function gambarspesifikasi() {
        return $this->hasMany(gambarspesifikasi::class);
    }
}
