<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Rating;
use App\Models\Kategori;
use App\Models\Background;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $kategori = Kategori::get();
        $produk = Produk::with(['size'])->withCount(['pesanan'])->get();
        $terjual = Produk::withCount('pesanan')->orderBy('terjual', 'DESC')->take(3);
        $background = Background::get();
        $keyword = $request->keyword;
        $produk = Produk::withCount('pesanan')->where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);
        return view('front.index', compact( 
            'kategori',
            'produk',
            'background',
            'terjual',
            'keyword'
        
        ));
    }

}
