<?php

namespace App\Http\Controllers;
use App\Models\Produk;
use App\Models\Setting;
use App\Models\Checkout;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use Faker\Provider\ar_EG\Person;

class PdfController extends Controller
{
    public function pdfpesananBaru($id)
    {
        $checkout = Checkout::with(['pengiriman', 'bank', 'pesanans'])->findOrFail($id);
        $pdf = PDF::loadView('front.pdf', compact('checkout'));
        return $pdf->stream('Invoice pesanan'. date('Y'). '.pdf');
    }

    public function pdfsedangDiproses($id)
    {
        $checkout = Checkout::with(['pengiriman', 'bank', 'pesanans'])->where('status','=', 'Sedang Diproses')->findOrFail($id);
        $pdf = PDF::loadView('front.pdf', compact('checkout'));
        return $pdf->stream('Invoice pesanan'. date('Y'). '.pdf');
    }

    public function pdfdalamPengiriman($id)
    {
        $checkout = Checkout::with(['pengiriman', 'bank', 'pesanans'])->findOrFail($id);
        $pdf = PDF::loadView('front.pdf', compact('checkout'));
        return $pdf->stream('Invoice pesanan'. date('Y'). '.pdf');
    }

    public function pdfselesai($id)
    {
        $checkout = Checkout::with(['pengiriman', 'bank', 'pesanans'])->findOrFail($id);
        $pdf = PDF::loadView('front.pdf', compact('checkout'));
        return $pdf->stream('Invoice pesanan'. date('Y'). '.pdf');
    }

    public function reportpdf(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $checkout = Checkout::with(['pengiriman', 'bank', 'pesanans'])->get();
        $totalPendapatan = Checkout::where('status', 'Telah Selesai')->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('checkout', function ($q) {
            $q->where('status', 'Telah Selesai');
        })->sum('harga_ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;
        $pdf = PDF::loadView('front.pdfreport', compact('checkout', 'totalPendapatan'));
        return $pdf->stream('Invoice pesanan'. date('Y'). '.pdf');
    }


 }
