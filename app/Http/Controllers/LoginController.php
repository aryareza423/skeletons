<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Http\Exceptions\InvalidData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index', [
        'title' => 'Login'
        ]);
    }

    public function authenticate(Request $request){
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);
        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            if (Auth::user()->is_admin == 1) {
                return redirect('/dashbord/index')->with('success', 'Login Berhasil, Halo' . ' ' .  auth()->user()->name);
            } else {
                return redirect('/')->with('success', 'Login Berhasil, Halo' . ' ' .  auth()->user()->name);
            }
        }
        return back()->with('loginEror', 'Login Gagal!');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/login'); 
    }
}

