<?php

namespace App\Http\Controllers;

use App\Models\GambarSpesifikasi;
use App\Models\SpesifikasiProduk;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SpesifikasiProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $spesifikasiproduk = SpesifikasiProduk::with(['produk'])->get(); 
        $keyword = $request->keyword;
        $spesifikasiproduk = SpesifikasiProduk::where(function ($q) use ($keyword) {
            $q->where('merek', 'LIKE', '%' . $keyword . '%');
            $q->where('negara_asal', 'LIKE', '%' . $keyword . '%');
            $q->where('model_sepatu', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('produk', function ($q) use ($keyword) {
                $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(100);
        return view('dashbord.spesifikasi.index', compact( 
            'spesifikasiproduk',
            'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = SpesifikasiProduk::all();
        $spesifikasiprodukProduk = Produk::all();
        return view('dashbord.spesifikasi.create', [
            'tambah' => $tambah,
             'dataProduk' => $spesifikasiprodukProduk  
             ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'merek' => 'required|string',
            'model_sepatu' => 'required|string', 
            'negara_asal' => 'required|string',
            'produk_id' => 'required'
        ]);
        
        $spesifikasiproduk = SpesifikasiProduk::create($validatedData);
        foreach($request->gambar_spesifikasi as $idx => $spek) {
            GambarSpesifikasi::create([
                'gambar' => $spek['gambar']->store('gambar-spesifikasi', 'public'),
                'spesifikasi_produk_id' => $spesifikasiproduk->id
            ]);
        }
        return redirect()->route('spesifikasiproduk.index')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SpesifikasiProduk  $spesifikasiproduk
     * @return \Illuminate\Http\Response
     */
    public function show(SpesifikasiProduk $spesifikasiproduk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SpesifikasiProduk  $spesifikasiproduk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allData = [
            'spesifikasiproduk' => SpesifikasiProduk::findOrFail($id),
            'dataProduk' =>  Produk::select('id', 'nm_produk')->get(),
            'gambar_spesifikasi' => GambarSpesifikasi::where('spesifikasi_produk_id', $id)->get()
        ];

        // dd($allData['gambar_spesifikasi']);
        // $data = SpesifikasiProduk::findOrFail($id);
        return view('dashbord.spesifikasi.edit', $allData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SpesifikasiProduk  $spesifikasiproduk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpesifikasiProduk $spesifikasiproduk)
    {
        $validatedData = $request->validate([
            'merek' => 'required|string',
            'model_sepatu' => 'required|string', 
            'negara_asal' => 'required|string',
            'produk_id' => 'required'
        ]);
        $tambah = SpesifikasiProduk::find($spesifikasiproduk->id);
        $tambah->update($validatedData);
        
        $old = [];
        foreach ($request->gambar_spesifikasi as $idx => $spek) {
            $old[] = $spek['gambar_id'];
        }
    
        GambarSpesifikasi::where('spesifikasi_produk_id', $spesifikasiproduk->id)->whereNotIn('id', $old)->delete();
        foreach($request->gambar_spesifikasi as $idx => $spek) {
            if (isset($spek['gambar'])) {
                GambarSpesifikasi::create([
                    'gambar' => $spek['gambar']->store('gambar-spesifikasi', 'public'),
                    'spesifikasi_produk_id' => $spesifikasiproduk->id
                ]);
            }
        }

        return redirect()->route('spesifikasiproduk.index')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SpesifikasiProduk  $spesifikasiproduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpesifikasiProduk $spesifikasiproduk)
    {
        $spesifikasiproduk->delete();
        return redirect()->back()->with('successdelete', 'Delete Successfull!');
    }
   
}
