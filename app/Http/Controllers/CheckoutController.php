<?php

namespace App\Http\Controllers;
use App\Models\Bank;
use App\Models\Kota;
use App\Models\Produk;
use App\Models\Pesanan;
use App\Models\Checkout;
use App\Models\Provinsi;
use App\Models\Keranjang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    public function index(Request $request){
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        $bank= Bank::get(); 
        $provinsis = Provinsi::get();
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);
        return view('front.checkout', compact(
            'keranjang',
            'total',
            'provinsis',
            'bank',
            'keyword'
        ));
    }

    public function konfirmasi(Request $request){
        $bank= Bank::find($request->bank_id); 
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $provinsi = Provinsi::find($request->provinsi_id);
        $kota = Kota::find($request->kota_id);
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);
        return view('front.konfirmasi', compact(
            'keranjang',
            'bank',
            'provinsi',
            'kota',
            'total',
            'keyword'
        ));
    }

    // public function pesanandibatalkan(Request $request, $id)
    // {
    //     $request->validate([
    //         'status' => 'required',
    //     ]);
    //     $checkout = Checkout::find($id);
    //     $checkout->update(['status' => $request->status]);
    //     return redirect('front.pesananSelesai');
    // }
   

    public function getOngkir(Request $request)
    {
        $data = $request->validate([
            'kota_id' => 'required',
            'courier' => 'required|in:jne,tiki,pos'
        ]);

        $cart = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $weight = 0;
        foreach ($cart as $keranjang) {
            $weight += $keranjang->produk->berat;
        }
        $response = Http::asForm()->withHeaders([
            'key' => getenv('RAJAONGKIR_API_KEY'),
        ])->post('https://api.rajaongkir.com/starter/cost', [
            'origin' => 444,
            'destination' => $request->kota_id,
            'courier' => $data['courier'],
            'weight' => $weight
        ]);

        return response()->json($response->json()['rajaongkir'], $response->status());
    }
}
