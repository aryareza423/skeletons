<?php

namespace App\Http\Controllers;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bank = Bank::all();
        $keyword = $request->keyword;
        $bank = Bank::where(function ($q) use ($keyword) {
            $q->where('nm_bank', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('atas_nama', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('no_rekening', 'LIKE', '%' . $keyword . '%');

        })->paginate(100);
        return view('dashbord.bank.index', compact( 
            'bank',
            'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Bank::all();
        return view('dashbord.bank.create', compact(
            'tambah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_bank' => 'required|string',
            'no_rekening' => 'required|string',
            'atas_nama' => 'required|string',
            'gambar' => 'required|file|max:1024'
        ]);
        $bank = $request->all();
        if($request->file('gambar')){
            $bank['gambar'] = $request->file('gambar')->store('Bank-image', 'public');
        }
        Bank::create($bank);
        return redirect('dashbord/bank')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        return view('dashbord.bank.edit', compact(
            'bank',
            
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        $validatedData = $request->validate([
            'nm_bank' => 'required|string',
            'no_rekening' => 'required|string',
            'atas_nama' => 'required|string',
            'gambar' => 'required|file|max:1024'
        ]); 
        if($request->file('gambar')){
            $validatedData['gambar'] = $request->file('gambar')->store('Bank-image', 'public');
        }
        $bank = Bank::find($bank->id);
        $bank->update($validatedData);
        return redirect ('dashbord/bank')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return redirect('dashbord/bank')->with('successdelete', 'Delete Successfull!');
    }
   
}


