<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

class ProdukController extends Controller
{
    public function index(Request $request)
    {
        $produk = Produk::with(['kategori'])->get(); 
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('deskripsi', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('price', 'LIKE', '%' . $keyword . '%');
            $q->orWhere('berat', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('kategori', function ($q) use ($keyword) {
                $q->where('nm_kategori', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate();
        return view('dashbord.produk.produk', compact(
            'produk',
            'keyword',
        ));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Produk::all();
        $produkKategori = Kategori::all();
        return view('dashbord.produk.create', [
            'tambah' => $tambah,
            'dataKategori' => $produkKategori
        ]);
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_produk' => 'required|string',
            'gambar' => 'file|max:1024',
            'deskripsi' => 'required|string',
            'berat'=>'required|integer',
            'price' => 'required|integer',
            'kategori_id' => 'required'
        ]);
        
        if($request->file('gambar')){
            $validatedData['gambar'] = $request->file('gambar')->store('produk-image', 'public');
        }
       $produk = Produk::create($validatedData);
       foreach($request->size as $idx => $prod) {
        Size::create([
            'stock' =>$prod['stock'],
            'size' =>$prod['size'],
            'produk_id' =>$produk->id
        ]);
       }
        
        return redirect('dashbord/produk')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allData = [
            'produk' => Produk::findOrFail($id),
            'kategori' =>  Kategori::select('id', 'nm_kategori', 'gambar')->get(),
            'size' => Size::where('produk_id', $id)->get()
        ];
        return view('dashbord.produk.edit', $allData);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        $validatedData = $request->validate([
            'nm_produk' => 'required|string',
            'gambar' => 'file|max:1024',
            'deskripsi' => 'required|string',
            'berat'=>'required|integer',
            'price' => 'required|integer',
            'kategori_id' => 'required'
        ]);
        if($request->file('gambar')){
            $validatedData['gambar'] = $request->file('gambar')->store('kategori-image', 'public');
        }
        $tambah = Produk::find($produk->id);
        $tambah->update($validatedData);

        $contains = [];
        // dd($request->size);
        foreach ($request->size as $size) {
            if (isset($size['id'])) {
                $contains[] = $size['id'];
                Size::where('id', $size['id'])->update($size);
            }
        }
        Size::where('produk_id', $produk->id)->whereNotIn('id', $contains)->delete();

        foreach ($request->size as $size) {
            if (!isset($size['id'])) {
                Size::create([
                    'stock' =>$size['stock'],
                    'size' =>$size['size'],
                    'produk_id' =>$produk->id
                ]);
            }
        }
        return redirect('dashbord/produk')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        $produk->delete();
        return redirect('dashbord/produk')->with('successdelete', 'Delete Successfull!');
    }
   
}


