<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Rating;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Models\SpesifikasiProduk;

class BlogController extends Controller
{
    public function index(Request $request, $id)
    {
        $produk = Produk::with(['spesifikasiproduks.gambarspesifikasi', 'size'])->withCount(['pesanan'])->find($id);
        $spesifikasiproduk = SpesifikasiProduk::where('produk_id', $produk->id)->first();
        $keyword = $request->keyword;
        $nilai_rating = Rating::where('produk_id', $id)->sum('rating');
        $banyak_rating = Rating::where('produk_id', $id)->count();
        $rating = 0;
        if ($banyak_rating > 0) {
            $rating = ($nilai_rating / $banyak_rating);
        }
        $ratingKomen = Rating::where('produk_id', $id)->get();
        $rating1 = Rating::where('produk_id', $id)->where('rating','1')->count();
        $rating2 = Rating::where('produk_id', $id)->where('rating','2')->count();
        $rating3 = Rating::where('produk_id', $id)->where('rating','3')->count();
        $rating4 = Rating::where('produk_id', $id)->where('rating','4')->count();
        $rating5 = Rating::where('produk_id', $id)->where('rating','5')->count();
        
        return view('front.detail', compact( 
            'produk',
            'spesifikasiproduk',
            'rating',
            'ratingKomen',
            'rating1',
            'rating2',
            'rating3',
            'rating4',
            'rating5',
            'banyak_rating',
            'keyword'
            
            
        ));
    }

}
