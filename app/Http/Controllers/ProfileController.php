<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Produk;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        
        $user = User::first();
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);
        return view('front.profile', compact(
            'user',
            'keyword'
           
        ));
    }

    public function profilesaya(Request $request)
    {
        $tambah = User::where('id', auth()->user()->id)->first();
     
        if (isset($tambah)) {
            $tambah->update($request->all());
        } else {
            User::create($request->all());
        }
        $user = $request->all();
        if (isset($request->avatar)) {
            $file = $request->file('avatar');
            $nama_file = time() . str_replace(" ", "-", $file->getClientOriginalName());
            $file->move('storage/avatar', $nama_file);
            $user['avatar'] = $nama_file;
        }
        if (isset($tambah)) {
            $tambah->update($user);
        } else {
            User::create($user);
        }

        return redirect('/profile')->with('successcreate', 'Berhasil Menambahkan Data Profile!');
    }

    // public function profilesaya(Request $request)
    // {
    //     auth()->user();
    //     return redirect('/profile')->with('successcreate', 'Create Successfull!');
    // }
}



