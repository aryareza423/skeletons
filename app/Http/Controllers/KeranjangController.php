<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Keranjang;
use Illuminate\Http\Request;

class KeranjangController extends Controller
{
    public function index(Request $request)
    {
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $kategori = Kategori::all();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);

        return view('front.keranjang', compact(
            'keranjang',
            'kategori',
            'total',
            'keyword'
        ));
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'produk_id' => 'required',
            'kuantitas' => 'required|integer|min:0',
            'size_id' => 'required'
        ]);

        $data['user_id'] = auth()->user()->id;

        if ($cek = Keranjang::where('produk_id', $request->produk_id)->where('size_id', $request->size_id)->where('user_id', auth()->user()->id)->first()) {
            $kuantitas = $cek->kuantitas + $data['kuantitas'];
            $size = Size::find($request->size_id);
            if ($kuantitas > $size->stock) {
                $kuantitas == $size->stock;
            }
            $cek->update([
                'kuantitas' => $kuantitas
            ]);
        } else {
            $data['user_id'] = auth()->user()->id;
            
            Keranjang::create($data);
        }

        return redirect()->back()->with('successkeranjang', 'Berhasil ditambahkan ke keranjang');
    }

    public function update(Request $request,  $id)
    {
        $tambah = Keranjang::find($id);
        $validatedData = $request->validate([
            'kuantitas' => 'required|integer|min:0',
        ]);
        $tambah->update($validatedData);
        return redirect()->back()->with('successupdate', 'Update Successfull!');
    }

    public function delete($id)
    {
        $tambah = Keranjang::find($id);
        $tambah->delete();
        return redirect()->back()->with('successdelete', 'Delete Successfull!');
    }
}

