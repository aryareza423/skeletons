<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Background;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index(Request $request, $id)
    {
        $kategori = Kategori::get();
        $produk = Produk::where('kategori_id', $id)->withCount('pesanan')->get();
        $keyword = $request->keyword;
        $produk = Produk::withCount('pesanan')->where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('kategori', function ($q) use ($keyword) {
                $q->where('nm_kategori', 'LIKE', '%' . $keyword . '%');
            });
        })->where('kategori_id', $id);
        if ($request->sort == 'terbaru') {
            $produk->orderBy('id', 'DESC');
        } else if ($request->sort == 'terendah') {
            $produk->orderBy('price', 'ASC');
        } else if ($request->sort == 'tertinggi') {
            $produk->orderBy('price', 'DESC');
        }
            
            $produk = $produk->where('nm_produk', 'LIKE', '%' . $keyword . '%')->paginate(100);
        return view('front.kategori', compact( 
            'kategori',
            'produk',
            'keyword',
        'id'
        ));
    }
}

