<?php

namespace App\Http\Controllers;
Use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('Register.index', [
            'title' => 'Register'
        ]);
    }
    public function store(Request $request)
    {
    $validateData = $request->validate([
    'nama' => 'required|max:255',
    'password' => 'required|min:5|max:255',
    'email' => 'required|email:dns|unique:users',
    'no_telpon' => 'required|numeric|min:10',
    'alamat' => 'required|max:255',
]);    
$validateData['password'] = Hash::make($validateData['password']);
User::create($validateData);
return redirect('/login')->with('success', 'Registration Berhasil! Silahkan Login');
    }
}
