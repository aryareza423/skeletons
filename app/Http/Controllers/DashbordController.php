<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Produk;
use App\Models\Rating;
use App\Models\pesanan;
use App\Models\Setting;
use App\Models\Checkout;
use App\Models\Pembayaran;
use App\Models\Pengiriman;
use Illuminate\Http\Request;

class DashbordController extends Controller
{
    public function index()
    {
        $user = User::where('is_admin','0')->paginate(5);
        $produk = Produk::get();
        $totalPendapatan = Checkout::where('status', 'Telah Selesai')->sum('total_harga');
        $totalOngkir = Pengiriman::whereHas('checkout', function ($q) {
            $q->where('status', 'Telah Selesai');
        })->sum('harga_ongkir');
        $totalPendapatan = $totalPendapatan - $totalOngkir;

        $totalPenjualan = pesanan::sum('kuantitas');
        $checkout = Checkout::get();
        $ratingKomen = Rating::where('produk_id')->get();


        return view('dashbord.index', compact(
            'produk',
            'totalPendapatan',
            'totalOngkir',
            'user',
            'checkout',
            'ratingKomen'
        ));
    }
}
