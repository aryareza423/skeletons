<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function ratingPembayaran(Request $request, $id)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'komen' => 'required|string',
            'gambar' => 'nullable|file',
            ('rating-'.$id) => 'required'
        ]);
        // $rating = $request->all();
        if($request->file('gambar')){
            $validatedData['gambar'] = $request->file('gambar')->store('rating-image', 'public');
        }
      
        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['produk_id'] = Produk::find
        ($id)->id;
        $validatedData['rating'] = $validatedData[('rating-'.$id)];
        if ($rating = Rating::where('user_id', auth()->user()->id)->where('produk_id', $validatedData['produk_id'])->first()) {
            $rating->update($validatedData);
        } else {
            Rating::create($validatedData);
        }
        return redirect()->back()->with('successcreate', 'Create Successfull!');
    }
}
