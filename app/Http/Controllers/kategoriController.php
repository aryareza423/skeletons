<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class kategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kategori = kategori::all();
        $keyword = $request->keyword;
        $kategori = Kategori::where(function ($q) use ($keyword) {
            $q->where('nm_kategori', 'LIKE', '%' . $keyword . '%');
        })->paginate(15);
        return view('dashbord.kategori.index', compact( 
            'kategori',
            'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Kategori::all();
        return view('dashbord.kategori.create', compact(
            'tambah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_kategori' => 'required|string',
            'gambar' => 'required|file|max:1024'
        ]);
        $kategori = $request->all();
        if($request->file('gambar')){
            $kategori['gambar'] = $request->file('gambar')->store('kategori-image');
        }
        Kategori::create($kategori);
        return redirect('dashbord/kategori')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        
        return view('dashbord.kategori.edit', compact(
            'kategori',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        $validatedData = $request->validate([
            'nm_kategori' => 'required|string',
            'gambar' => 'required|file|max:1024'
        ]);
        if($request->file('gambar')){
            $validatedData['gambar'] = $request->file('gambar')->store('kategori-image', 'public');
        }
        $tambah = Kategori::find($kategori->id);
        $tambah->update($validatedData);
        return redirect('dashbord/kategori')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        $kategori->delete();
        return redirect('dashbord/kategori')->with('successdelete', 'Delete Successfull!');
    }
   
}
