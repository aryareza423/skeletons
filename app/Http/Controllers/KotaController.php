<?php

namespace App\Http\Controllers;

use App\Models\Kota;
use App\Models\Provinsi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $data = Kota::with(['provinsi'])->get(); 
        $keyword = $request->keyword;
        $data = Kota::where(function ($q) use ($keyword) {
            $q->where('nm_kota', 'LIKE', '%' . $keyword . '%');

            $q->orWhereHas('provinsi', function ($q) use ($keyword) {
                $q->where('nm_provinsi', 'LIKE', '%' . $keyword . '%');
            });
        })->paginate(200);
        return view('dashbord.kota.index', compact( 
            'data',
            'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Kota::all();
        $dataProvinsi = Provinsi::all();
        return view('dashbord.kota.create', [
            'tambah' => $tambah,
            'dataProvinsi' => $dataProvinsi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'komentar' => 'required|string',
            'provinsi_id' => 'required'

            
        ]);
        Kota::create($validatedData);
        return redirect('dashbord/kota')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function show(kota $kota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function edit(Kota $kota)
    {

        return view('dashbord.kota.edit', compact(
            'kota',
            
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kota $id)
    {
        $tambah = Kota::find($id);
        $validatedData = $request->validate([
            'nm_kota' => 'required|string',
            'provinsi_id' => 'required'

          

        ]);
        $data = $request->all();
        $tambah->update($data);
        return redirect('dashboard/kota')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kota  $kota
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kota $kota)
    {
        $kota->delete();
        return redirect('dashbord/kota')->with('successdelete', 'Delete Successfull!');
    }
   
    public function get($provinsi_id) {
        $kotas = Kota::where('provinsi_id', $provinsi_id)->get();
        return response()->json($kotas);
    }
}
