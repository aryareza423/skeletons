<?php

namespace App\Http\Controllers;
use App\Models\Bank;
use App\Models\Kota;
use App\Models\Size;
use App\Models\Produk;
use App\Models\Rating;
use App\Models\Pesanan;
use App\Models\Checkout;
use App\Models\Provinsi;
use App\Models\Keranjang;
use App\Models\Pengiriman;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;


//pesanan baru
class PengirimanController extends Controller
{
    public function pesananbaru(Request $request){
    $checkout = Checkout::where('status', '=', 'Pesanan Baru')->get();
    return view('dashbord.pesananbaru.index', compact(
        'checkout',
    ));
    }

    public function detailPesanan(Request $request, $id){
        $checkout = Checkout::find($id);
        $bank = Bank::find($id);
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        return view('dashbord.pesananbaru.detailPesanan', compact(
            'checkout',
            'bank',
            'keranjang',
            'total'
        ));
    }

    public function updateStatuspesananbaru(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);
        $checkout = Checkout::find($id);
        $checkout->update(['status' => $request->status]);
        return redirect('/dashbord/pesananbaru');
    }



    //proses pesanan

    public function sedangdiproses(Request $request){
        $checkout = Checkout::where('status', '=', 'Sedang DiProses')->get();
        return view('dashbord.sedangdiproses.index', compact(
            'checkout',
            ));
    }

    public function detailProses(Request $request, $id){
        $checkout = Checkout::find($id);
        $bank = Bank::find($id);
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        return view('dashbord.sedangdiproses.detailProses', compact(
            'checkout',
            'bank',
            'keranjang',
            'total'
        ));
    }

    public function updateStatussedangdiproses(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);
        $checkout = Checkout::find($id);
        $checkout->update(['status' => $request->status]);
        return redirect('/dashbord/sedangdiproses');
    }


    //pengiriman pesanan
    public function dalampengiriman(Request $request){
        $checkout = Checkout::where('status', '=', 'Dalam Pengiriman')->get();
        return view('dashbord.dalampengiriman.index', compact(
            'checkout',
            ));
    }

    public function detailPengiriman(Request $request, $id){
        $checkout = Checkout::find($id);
        $bank = Bank::find($id);
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        return view('dashbord.dalampengiriman.detailPengiriman', compact(
            'checkout',
            'bank',
            'keranjang',
            'total'
        ));
    }
    public function updatestatusdalampengiriman(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);
        $checkout = Checkout::find($id);
        $checkout->update(['status' => $request->status]);
        return redirect('/dashbord/dalampengiriman');
    }

    public function updatestatususer(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);
        $checkout = Checkout::find($id);
        $checkout->update(['status' => $request->status]);
        return redirect('/pesanan');
    }

    //selesai pesanan
    public function telahselesai(Request $request){
        $checkout = Checkout::where('status', '=', 'Telah Selesai')->get();
        return view('dashbord.telahselesai.index', compact(
            'checkout',
            ));
    }

    public function detailSelesai(Request $request, $id){
        $checkout = Checkout::find($id);
        $bank = Bank::find($id);
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        return view('dashbord.telahselesai.detailSelesai', compact(
            'checkout',
            'bank',
            'keranjang',
            'total'
        ));
    }

    //pembayaran
    public function Pembayaran(Request $request){
        $total = 0;
        $keranjang = Keranjang::with('produk')->where('user_id', auth()->user()->id)->get();
        if ($keranjang->count() == 0) {
            return redirect('/keranjang');
        }
        $total = 0;
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        
        $checkout = Checkout::create([           
            'total_harga' => $total, 
            'user_id' => auth()->user()->id,
            'bank_id' => $request->bank_id,
            'status' => '1'
        ]);

        
        $pengiriman = Pengiriman::create([
            'checkout_id' => $checkout->id,
            'bank_id' => $request->bank_id,
            'kota_id' => $request->kota_id,
            'provinsi_id' => $request->provinsi_id,
            'harga_ongkir' => $request->layanan,
            'nama' => $request->nama,
            'no_hp' => $request->no_hp,
            'nm_kurir' => $request->courier,
            'jenis_kurir' => $request->courier,
            'nm_penerima' => $request->nm_penerima,
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
        ]);

        foreach ($keranjang as $i) {
            Pesanan::create([
                'checkout_id' => $checkout->id,
                'produk_id' => $i->produk->id,
                'size' => $i->size_id,
                'kuantitas' => $i->kuantitas,
                'sub_total' => $i->kuantitas * $i->price
            ]);
            $produk = Size::find($i->size_id);
            $produk->update(['stock' => $produk->stock - $i->kuantitas]);
            $i->delete();
        }
        return redirect('/pembayaran/' . $checkout->id);
    }
    

    //detailpembayaran
    public function detailPembayaran(Request $request, $id){
        $checkout = Checkout::where('id', $id)->first();
        $bank = Bank::find('id');
        $keranjang = Keranjang::with('produk', 'size')->where('user_id', auth()->user()->id)->get();
        $total = 0;
        $rating = Rating::where('id', $id);
        foreach ($keranjang as $i) {
            $total += $i->subtotal;
        }
        $keyword = $request->keyword;
        $produk = Produk::where(function ($q) use ($keyword) {
            $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
        })->paginate(50);
        return view('front.detailPembayaran', compact(
        'checkout',
        'bank',
        'keranjang',
        'total',
        'rating',
        'keyword'
        ));
    }

    public function update(Request $request, $id){
        $data['bukti_pembayaran'] =$request->file('bukti_pembayaran');
        if($request->file(['bukti_pembayaran'])){
            $data['bukti_pembayaran'] = $request->file('bukti_pembayaran')->store('bukti_pembayaran', 'public');
        }
        Checkout::where('id', $id)->update($data);
        $checkout = Checkout::where('id', $id)->find($id);
        return back()->with('success', 'Bukti Pembayaran Berhasil Dikirim');

    }

    //pesanan
  public function pesananSelesai(Request $request){
    $keyword = $request->keyword;
    $produk = Produk::where(function ($q) use ($keyword) {
        $q->where('nm_produk', 'LIKE', '%' . $keyword . '%');
    })->paginate(50);
        $checkout = Checkout::where('status', '!=', 'Telah Selesai')->get();
        $selesai = Checkout::where('status', '=', 'Telah Selesai')->get();
        return view('front.pesananSelesai', compact(
            'checkout',
            'selesai',
            'keyword'
        ));
        }
        
    
}
