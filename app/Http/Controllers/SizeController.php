<?php

namespace App\Http\Controllers;

use App\Models\Size;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $size = Size::with(['produk'])->get(); 
        return view('dashbord.size.index', compact( 
            'size'
        ));
          
       
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Size::all();
        $size = Produk::all();

        return view('dashbord.size.create', [
            'tambah' => $tambah,
            'dataProduk' => $size  

             ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'size' => 'required|string',
            'stock' => 'required|integer',
            'produk_id' => 'required'

        ]);
        Size::create($validatedData);
        return redirect('dashbord/size')->with('successcreate', 'Create Successfull!');
        }
       
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Size  $Size
     * @return \Illuminate\Http\Response
     */
    public function show(Size $Size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Size  $Size
     * @return \Illuminate\Http\Response
     */
    public function edit(Size $id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Size  $Size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Size $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Size  $Size
     * @return \Illuminate\Http\Response
     */
    public function destroy(Size $Size)
    {
      
    }
   
}
