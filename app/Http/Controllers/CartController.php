<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    public function index(){
        $cart = Cart::Content();
        $total_berat =0;
        foreach ($cart as $item){
            $total_berat +=$item->weight * $item->qty;
        }
        dd($cart);
        return view('front.keranjangitem', compact('cart', 'total_berat'));
    }

    public function store(Request $request)
    {
        $produk = Produk::where('id', '=', $request->id)->first();

        Cart::add([
            'id' => $produk->id, 
            'name' => $produk->nm_produk, 
            'qty' => $request->input('quantity'), 
            'price' => $produk->price, 
            'weight' => $produk->berat, 
            'options' => [
                'gambar' => $request->input('gambar'),
                'stok' => $produk->stok
            ],
        ]);
        return redirect('keranjang')->with('success', 'cart ditambahkan');
    }

 


}
