<?php

namespace App\Http\Controllers;

use App\Models\Provinsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Provinsi::all();
        $keyword = $request->keyword;
        $data = Provinsi::where(function ($q) use ($keyword) {
            $q->where('nm_provinsi', 'LIKE', '%' . $keyword . '%');
        })->paginate(200);
        return view('dashbord.provinsi.index', compact( 
            'data',
            'keyword'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tambah = Provinsi::all();
        return view('dashbord.provinsi.create', compact(
            'tambah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nm_provinsi' => 'required|string',
            
        ]);
        $data = $request->all();
        Provinsi::create($data);
        return redirect('dashbord/provinsi')->with('successcreate', 'Create Successfull!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provinsi  $Provinsi
     * @return \Illuminate\Http\Response
     */
    public function show(Provinsi $Provinsi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provinsi  $Provinsi
     * @return \Illuminate\Http\Response
     */
    public function edit(Provinsi $Provinsi)
    {

        return view('dashbord.Provinsi.edit', compact(
            'provinsi',
            
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provinsi  $Provinsi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provinsi $id)
    {
        $tambah = Provinsi::find($id);
        $validatedData = $request->validate([
            'nm_provinsi' => 'required|string',
          

        ]);
        $data = $request->all();
        $tambah->update($data);
        return redirect('dashboard/Provinsi')->with('successupdate', 'Update Successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provinsi  $Provinsi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provinsi $Provinsi)
    {
        $Provinsi->delete();
        return redirect('dashbord/provinsi')->with('successdelete', 'Delete Successfull!');
    }
   
}
