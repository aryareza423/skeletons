<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KotaController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\KatalogController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\DeliveryController;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\ProvinsiController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\KeranjangController;
use App\Http\Controllers\BackgroundController;
use App\Http\Controllers\KonfirmasiController;
use App\Http\Controllers\PengirimanController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\SpesifikasiProdukController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/detail/{id}', [BlogController::class, 'index']);
Route::get('/kategori/{id}', [CatalogController::class, 'index']);


Route::post('/konfirmasi', [CheckoutController::class, 'konfirmasi'])->middleware('auth');

Route::post('/pembayaran', [PengirimanController::class, 'pembayaran'])->middleware('auth');
Route::get('/pembayaran/{id}', [PengirimanController::class, 'detailPembayaran'])->middleware('auth');
Route::post('/pembayaranrating/{id}', [RatingController::class, 'ratingPembayaran'])->middleware('auth');

Route::get('/pesanan', [PengirimanController::class, 'pesananSelesai'])->middleware('auth');

Route::post('/pembayaran/{id}/uplod-bukti', [PengirimanController::class, 'update'])->middleware('auth');
Route::get('/checkout', [CheckoutController::class, 'index'])->middleware('auth');
Route::get('/kota/{provinsi_id}', [KotaController::class, 'get']);
Route::post('/checkout/get-ongkir', [CheckoutController::class, 'getOngkir']);
Route::post('/user/selesai/{id}/updatestatusselesai', [PengirimanController::class, 'updatestatususer']);



//menu profile
Route::get('/profile', [ProfileController::class, 'index'])->middleware('auth');
Route::post('/profile/profilesaya', [ProfileController::class, 'profilesaya'])->middleware('auth');

//menu login
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);


//register
Route::get('/register', [RegisterController::class, 'index'])->name('register')->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

//front

Route::resource('/dashbord/index', DashbordController::class);
//Dashbord admin
Route::resource('/dashbord/kategori', KategoriController::class);
Route::resource('/dashbord/produk', ProdukController::class);
Route::resource('/dashbord/spesifikasiproduk', SpesifikasiProdukController::class);
Route::resource('/dashbord/kota', KotaController::class);
Route::resource('/dashbord/provinsi', ProvinsiController::class);
Route::resource('/dashbord/background', BackgroundController::class);   
Route::resource('/dashbord/bank', BankController::class);
Route::resource('/dashbord/size', SizeController::class);



//pesanan dahbord
Route::get('/dashbord/pesananbaru', [PengirimanController::class, 'pesananbaru']);
Route::get('/dashbord/pesananbaru/{id}/detail', [PengirimanController::class, 'detailPesanan']);
Route::post('/dashbord/pesananbaru/{id}/updatestatus', [PengirimanController::class, 'updatestatuspesananbaru']);

//proses pesanan
Route::get('/dashbord/sedangdiproses', [PengirimanController::class, 'sedangdiproses']);
Route::get('/dashbord/sedangdiproses/{id}/detail', [PengirimanController::class, 'detailProses']);
Route::post('/dashbord/sedangdiproses/{id}/updatestatus', [PengirimanController::class, 'updatestatussedangdiproses']);

//pengiriman pesanan
Route::get('/dashbord/dalampengiriman', [PengirimanController::class, 'dalampengiriman']);
Route::get('/dashbord/dalampengiriman/{id}/detail', [PengirimanController::class, 'detailPengiriman']);
Route::post('/dashbord/dalampengiriman/{id}/updatestatus', [PengirimanController::class, 'updatestatusdalampengiriman']);
 
//pesanan selesai
Route::get('/dashbord/telahselesai', [PengirimanController::class, 'telahselesai']);
Route::get('/dashbord/telahselesai/{id}/detail', [PengirimanController::class, 'detailSelesai']);

//PDF
Route::get('/dashbord/telahselesai/detail-pdf/{id}', [PdfController::class, 'pdfselesai']);
Route::get('/dashbord/pdfreport/detail-pdf', [PdfController::class, 'reportpdf']);
Route::get('/dashbord/pesananbaru/detail-pdf/{id}', [PdfController::class, 'pdfpesananBaru']);
Route::get('/dashbord/dalampengiriman/detail-pdf/{id}', [PdfController::class, 'pdfdalamPengiriman']);
Route::get('/dashbord/sedangdiproses/detail-pdf/{id}', [PdfController::class, 'pdfsedangDiproses']);

//keranjang
Route::get('/keranjang', [KeranjangController::class, 'index'])->middleware('auth');
Route::post('/keranjang/create', [KeranjangController::class, 'store'])->name('keranjang.store')->middleware('auth');
Route::post('/keranjang/{id}', [KeranjangController::class, 'update'])->middleware('auth');
Route::delete('/keranjang/{id}', [KeranjangController::class, 'delete'])->middleware('auth');









