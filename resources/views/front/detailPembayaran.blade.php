
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <style>
        
    .rating {
    margin-top: 40px;
    border: none;
    float: left;
    }

    .rating > label {
    color: #90A0A3;
    float: right;
    }


    .rating > input {
    display: none;
    }

    .rating > input:checked ~ label,
    .rating:not(:checked) > label:hover,
    .rating:not(:checked) > label:hover ~ label {
    color: #F79426;
    }

    .rating > input:checked + label:hover,
    .rating > input:checked ~ label:hover,
    .rating > label:hover ~ input:checked ~ label,
    .rating > input:checked ~ label:hover ~ label {
    color: #FECE31;
    }
    </style>
    <title>Pembayaran</title>
</head>
<body>
  @include('dashbord.layouts.navbar')
  <section class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__text">
                    <div class="breadcrumb__links">
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>


  <section class="checkout spad">
   <div class="container">
    
    <div class="row">
        <div class="col-lg-12" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2" >
            <h5>Detail Pesanan</h5>
            @if ($checkout->status == 'Pesanan Baru')
            <span class="badge badge-warning">Menunggu Pembayaran</span>
            @elseif ($checkout->status == 'Sedang DiProses')
            <span class="badge badge-primary">Sudah Dikonfirmasi, Sedang DiProses Pesanan Anda</span>
            @elseif ($checkout->status == 'Dalam Pengiriman')
            <span class="badge badge-primary">Dalam Pengiriman</span>
            @elseif ($checkout->status == 'Telah Selesai')
            <span class="badge badge-success">Selesai</span>
            @endif
        </div>
        
        
    </div>
    <div class="row">
        <div class="col-lg-4 mt-1" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2">
            <h5 class="pesan" style="color:#b1adad">Dipesan Kepada:</h5>
            <h5 class="mt-5">Nama :{{ $checkout->pengiriman->nama }}</h5>
            <h6 class="mt-2">No Hp :{{ $checkout->pengiriman->no_hp }}</h6>
            <h6 class="mt-2">Nama Penerima : {{ $checkout->pengiriman->nm_penerima }} </h6>
            <h6 class="mt-2">Jenis Pengiriman :{{ $checkout->pengiriman->jenis_kurir }}</h6>    
            <h6 class="mt-2" >Jenis Pembayaran :{{ $checkout->bank->nm_bank }}</h6>    
             <img src="{{ asset('storage/' . $checkout->bank->gambar) }}" alt="">
        </div>
        <div class="col-lg-4 mt-1" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2">
            <h5 class="pembayaran" style="color:#b1adad">Alamat:</h5>
            <h5 class="mt-5">Alamat : {{ $checkout->pengiriman->alamat }}   </h5>
            <h6 class="mt-2">Provinsi {{ $checkout->pengiriman->provinsi->nm_provinsi }}</h6>
            <h6 class="mt-2">Kota {{ $checkout->pengiriman->kota->nm_kota }}</h6>
            <h6 class="mt-2">Kode Pos :{{ $checkout->pengiriman->kode_pos }}</h6>
           
        </div>
        <div class="col-lg-4 mt-1" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2">
            <h5 class="pembayaran" style="color:#b1adad">Info Pembayaran:</h5>
            <h5 class="mt-5">Total Tagihan</h5>
            <h3 class="mt-2 font-weight-bold">   <span>{{number_format( intval(str_replace(',', '', $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</span>
            </h3>
            <h5 class="mt-2">Metode Pembayaran:</h5>
            <h4 class="mt-2">Transfer Bank</h4>
            @if ($checkout->status == 'Pesanan Baru')
            {{-- @elseif ($checkout->status == 'Sedang DiProses') --}}
            <h6 class="mt-4">Sudah Melakukan Pembayaran atau Salah Uplod Bukti?</h6>
            <label class="mt-3" style="color:black">
                <form action="/pembayaran/{{ $checkout->id }}/uplod-bukti" method="POST" enctype="multipart/form-data">
                  @csrf 
                  @if (session()->has ('success'))
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                      {{ session('success') }}
                      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
                  </div>
                  @endif
                <input type="file" name="bukti_pembayaran" id="bukti_pembayaran" required>
                <button type="submit" class="btn btn-primary mt-3">Kirim</button>
            </form>
            
            </label>
            @endif
           
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2;background:#fff">
            <div class="checkout__order" style="background: #fff;padding:0px">
                <h4 class="order__title">Daftar Barang</h4>
                <div class="checkout__order__products">Subtotal</div>
                <ul class="checkout__total__products">
                    @foreach ($checkout->pesanans as $item)
                    {{-- @dd($item->sizes); --}}
                    <li>{{ $item->produk->nm_produk }}-{{ $item->sizes->size }} <span>Rp.{{ number_format($item->produk->price * $item->kuantitas, 0, ',', '.') }}</span></li>  
                        @if ($checkout->status == 'Telah Selesai')
                        @if (!$item->produk->is_rated)
                        <button type="button" class="badge badge-primary mb-3" data-bs-toggle="modal" data-bs-target="#modal-rating-{{ $item->produk_id }}">
                           Berikan Penilaian Produk
                        </button>
                        <div class="modal fade" tabindex="-1" id="modal-rating-{{ $item->produk_id }}">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Penilaian Produk</h5>
                        
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2 " data-bs-dismiss="modal" aria-label="Close">
                                            <span class="svg-icon svg-icon-2x"></span>
                                        </div>
                                        <!--end::Close-->
                                    </div>
                        
                                    <div class="modal-body ">
                                           <img src="{{ asset('storage/' . $item->produk->gambar) }}" alt="" class="w-25 mt-3">
                                           
                                           <form action="{{ url('/pembayaranrating/'. $item->produk->id) }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="fw-bold mt-3" style="color:black">{{ $item->produk->nm_produk}}</div> <span>  <div class="form-floating">
                                                    <div class="rating mt-2">
                                                        <input type="radio" id="star5-{{ $item->produk_id }}" name="rating-{{ $item->produk_id }}" value="5" />
                                                        <label class="star" for="star5-{{ $item->produk_id }}" title="Luar Biasa " aria-hidden="true">
                                                            <i class="fa fa-star"></i>
                                                        </label>
                                                        <input type="radio" id="star4-{{ $item->produk_id }}" name="rating-{{ $item->produk_id }}" value="4" />
                                                        <label class="star" for="star4-{{ $item->produk_id }}" title="Bagus Banget" aria-hidden="true">
                                                            <i class="fa fa-star"></i>
                                                        </label>
                                                        <input type="radio" id="star3-{{ $item->produk_id }}" name="rating-{{ $item->produk_id }}" value="3" />
                                                        <label class="star" for="star3-{{ $item->produk_id }}" title="Sangat Bagus" aria-hidden="true">
                                                            <i class="fa fa-star"></i>
                                                        </label>
                                                        <input type="radio" id="star2-{{ $item->produk_id }}" name="rating-{{ $item->produk_id }}" value="2" />
                                                        <label class="star" for="star2-{{ $item->produk_id }}" title="Bagus" aria-hidden="true">
                                                            <i class="fa fa-star"></i>
                                                        </label>
                                                        <input type="radio" id="star1-{{ $item->produk_id }}" name="rating-{{ $item->produk_id }}" value="1" />
                                                        <label class="star" for="star1-{{ $item->produk_id }}" title="Jelek" aria-hidden="true">
                                                            <i class="fa fa-star"></i>
                                                        </label>
                                                    </div>

                                                    <span>
                                                    <input type="file" name="gambar" id="gambar" class="col text-dark mt-3 px-0">
                                                </span>
                                                    <textarea class="form-control mt-3" name="komen" placeholder="Komentar "  id="floatingTextarea2" style="height: 100px"></textarea>
                                                    
                                                </div>
                                            </span>
            
                                              
                                            </div>
                                
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Kirim</button>
                                            </div>
                                         </form>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endif
                 
                    
                    @endforeach
                    <li>Ongkir <span>Rp.{{ number_format( $checkout->pengiriman->harga_ongkir, 0, ',', '.') }}</span></li>
                </ul>
                <ul class="checkout__total__all">
                    <li>Total   <span>{{number_format( intval(str_replace(',', '', $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</span></li>
                </ul>
            </div>
        </div>
    </div>
   </div>
  </section>
    
@include('dashbord.layouts.footer')




{{-- <script src="https://use.fontawesome.com/7ad89d9866.js"></script> --}}
<script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
<script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
<script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
<script src="{{ asset('assets/plugins/global/plugins.bundle.js')  }}"> </script>
<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('assets/js/scripts.bundle.js')}}"></script>
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('assets/js/custom/utilities/modals/create-app.js') }}"></script>
<script src="{{ asset('assets/js/custom/utilities/modals/users-search.js') }}"></script>
</body>
</html>