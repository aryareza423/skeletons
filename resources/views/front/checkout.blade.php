<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <title>Checkout</title>

    <style>
    .pilih-bank{
    display: block;
    width: 100%;
    cursor: pointer;
        }
    .pilih-bank img:nth-child(2) {
    outline: 1px solid #c2c2c2;
    border-radius: 4px;
    padding: 0.5rem 1rem;
    text-align: center;
    width: 100%;
    font-weight: 700;
}
.pilih-bank input {
    display: none;
}
.pilih-bank input:checked ~ img:nth-child(2) {
    outline: 2px solid #6380ff;
}
    </style>
</head>
<body>
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    @include('dashbord.layouts.navbar')

    <!-- Header Section End -->

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__text">
                        <h4></h4>
                        <div class="breadcrumb__links">
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            <div class="checkout__form">
                <form method="POST" action="/konfirmasi">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8 col-md-6">
                            <h6 class="checkout__title">Isi Data</h6>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nama<span>*</span></p>
                                        <input type="text" name="nama" id="nama" value="{{ auth()->user()->nama }}" autocomplete="off"required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>No Hp<span>*</span></p>
                                        <input type="text" name="no_hp" id="no_hp" value="{{ auth()->user()->no_telpon }}" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Alamat<span>*</span></p>
                                        <input type="text" name="alamat" id="alamat" autocomplete="off" value="{{ auth()->user()->alamat }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Provinsi<span>*</span></p>
                                        <select class="nice-select" name="provinsi_id" id="provinsi_id" required>
                                            <option value="">Pilih Provinsi</option>
                                            @foreach ($provinsis as $provinsi)
                                                <option value="{{ $provinsi->id }}">{{ $provinsi->nm_provinsi }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Kota<span>*</span></p>
                                        <select class="nice-select" name="kota_id" id="kota_id" required disabled>
                                            <option value="">Pilih Kota</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 mt-3">
                                    <div class="checkout__input">
                                        <p>Kode Pos<span>*</span></p>
                                        <input type="text" name="kode_pos" id="kode_pos" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 mt-3">
                                    <div class="checkout__input">
                                        <p>Nama Penerima<span>*</span></p>
                                        <input type="text" name="nm_penerima" id="nm_penerima" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 mt-3">
                                    <div class="checkout__input">
                                        <p>Jenis Pengiriman<span>*</span></p>
                                        <select class="nice-select" name="courier" id="courier" required>
                                            <option value="">Pilih Pengiriman</option>
                                            <option value="jne">JNE</option>
                                            <option value="tiki">TIKI</option>
                                            <option value="pos">POS INDONESIA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 mt-3">
                                    <div class="checkout__input">
                                        <p>Layanan (Ongkir)<span>*</span></p>
                                        <select class="nice-select" name="layanan" id="layanan" required disabled>
                                            <option value="">Pilih Layanan</option>
                                        </select>
                                    </div>
                                </div>
                               
                            </div> 
                          <div class="row mt-3">
                            <div class="col-lg-12">
                            <span>Anda dapat melakukan pembayaran Dengan Memilih Bank Dibawah Ini !</span>
                           </div>
                          </div>
                          <div class="row">
                            
                            @foreach ($bank as $item)
                            <div class="col-lg-6 mt-3">
                                <label class="pilih-bank" for="bank-{{ $item->id }}">
                                    <input type="radio" name="bank_id" id="bank-{{ $item->id }}" value="{{ $item->id }}">
                                    <img src="{{ asset('storage/' . $item->gambar) }}" alt="">
                                    <h6 class="mt-3">Nama Bank : {{ $item->nm_bank }}</h6>
                                    <h6 class="mt-3">No Rekening Bank : {{ $item->no_rekening }}</h6>
                                    <h6 class="mt-3">Atas Nama : {{ $item->atas_nama }}</h6>
                                </label>
                            </div>
                            @endforeach
                           
                          </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="checkout__order">
                                <h4 class="order__title">Orderan</h4>
                                <div class="checkout__order__products">Subtotal</div>
                                <ul class="checkout__total__products">
                                    @foreach ($keranjang as $item)
                                      <li>{{ $item->produk->nm_produk }}-{{ $item->size->size }} <span>Rp.{{ number_format($item->produk->price * $item->kuantitas , 0, ',', '.') }}</span></li>
                                    @endforeach
                                </ul>
                                <ul class="checkout__total__all">
                                    <li>Total <span>{{ number_format($total, 0, ',', '.') }}</span></li>
                                </ul>
                             <button type="submit"  class="site-btn">CHECKOUT</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->

    <!-- Footer Section Begin -->
      @include('dashbord.layouts.footer')

    <!-- Search End -->
    
  <script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
  <script src="{{ asset('assets/js/mixitup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>

  <script>
    document.querySelector('[name="provinsi_id"]').addEventListener('change', function () {
        $.ajax({
            url: '/kota/' + this.value,
            success: function (data) {
                document.querySelector('[name="kota_id"]').innerHTML = data.map(kota => `
                    <option value="${kota.id}">${kota.nm_kota}</option>
                `).join('')

                document.querySelector('[name="kota_id"]').removeAttribute('disabled');
            }
        })
    })
  </script>
   <script>
    document.querySelector('[name="courier"]').addEventListener('change', function () {
        $.ajax({
            url: '/checkout/get-ongkir',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                // weight: 
                courier: document.querySelector('[name="courier"]').value,
                kota_id: document.querySelector('[name="kota_id"]').value,
            },
            success: function({
                results: data
            }) {
                document.querySelector('#layanan').innerHTML = data[0].costs.map(item => `
                    <option id="ongkir" value="${item.cost[0].value}">${item.service} | Rp ${item.cost[0].value} | ${item.cost[0].etd.replace(' HARI', '')} Hari</option>
                `);
                document.querySelector('#layanan').removeAttribute('disabled');
            }
        })
    });

    function changeOngkirText(cost) {
        document.querySelector('.ongkir-text').innerText = cost.cost[0].value;
        document.querySelector('.total-text').innerText = parseInt({{ $total }}) +
            cost.cost[0].value;

        document.querySelector('input[name="service"]').value = `${cost.service} (${cost.description})`;
    }
</script>
  
  
</body>
</html>