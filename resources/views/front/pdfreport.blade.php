<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF </title>
</head>
<body>
    <table style="width:100%">
    <tr>
        <td  style="width: 50%; font-size:2rem; ">
        Pendapatan Penjualan 
        </td>
    </tr>
    </table>
<table class="produk" style="width:100%; margin-top:3rem;border:1px solid black;border-collapse: collapse; ">
    <tr>
<th style="border:1px solid black;"> Nama</th>
<th style="border:1px solid black;">Produk</th>
<th style=" border:1px solid black">Total harga</th>
</tr>
@foreach ($checkout as $item)
<tr>
    <td style="border:1px solid black">
        <div style="margin-top:1rem;margin-bottom:1rem; margin-left:2rem; ">{{ $item->pengiriman->nama }}</div>
    </td>
    @foreach ($item->pesanans as $value)
    <td style="text-align: center;border:1px solid black">{{ $value->produk->nm_produk }}</td>
    @endforeach
    <td style="text-align: center;border:1px solid black">Rp.{{number_format( intval(str_replace(',', '.', $item->total_harga))) }}</td>
</tr>
@endforeach

<tr>
    <th style="text-align: center;border:1px solid black"></th>
    <th style="text-align: center;border:1px solid black"></th>
    <th style="text-align: center;border:1px solid black">Total Pendapatan : Rp.{{number_format( intval(str_replace(',', '.', $totalPendapatan))) }}</th>
</tr>

</table>


</body>
</html>