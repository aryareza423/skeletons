<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <title>Pembayaran</title>
</head>
<body>
  @include('dashbord.layouts.navbar')
  <section class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__text">
                    <div class="breadcrumb__links">
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="checkout spad">
   <div class="container">
    <form method="POST">
        @csrf
    <div class="row">
        <div class="col-lg-12" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2" >
            <h5>Detail Pesanan</h5>
            <h6 class="mt-2">Menunggu Pembayaran</h6>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mt-1" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2">
            <h5 class="pesan" style="color:#b1adad">Dipesan Kepada:</h5>
            <h5 class="mt-5 font-weight-bold fa-2x">Nama :{{ request()->nama }}</h5>
            <h6 class="mt-2">No Hp :{{ request()->no_hp }}</h6>
            <h5 class="mt-2">Alamat : {{ request()->alamat }} <span>{{ request()->nm_kota }}</span>, Provinsi {{ request()->nm_provinsi }}</h5>
            <h6 class="mt-2">Kode Pos :{{ request()->kode_pos }}</h6>
            <h6 class="mt-2">Nama Penerima : {{ request()->nm_penerima }}</h6>
            <h6 class="mt-2">Jenis Pengiriman :{{ request()->courier }}</h6>    
            <h6 class="mt-2" >Jenis Pembayaran :{{ $bank->nm_bank }}</h6>    
             <img src="{{ asset('storage/' . $bank->gambar) }}" alt="">
        </div>
        <div class="col-lg-6 mt-1" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2">
            <h5 class="pembayaran" style="color:#b1adad">Info Pembayaran:</h5>
            <h5 class="mt-5">Total Tagihan</h4>
            <h3 class="mt-2 font-weight-bold">{{number_format( intval(str_replace(',', '', Cart::priceTotal())) + request()->layanan) }}</h3>
            <h5 class="mt-2">Metode Pembayaran:</h5>
            <h4 class="mt-2">Transfer Bank</h4>
            <h6 class="mt-4">Sudah Melakukan Pembayaran atau Salah Uplod Bukti?</h6>
            <label class="mt-3" style="color:black">
                <span></span>
                <input type="file" name="bukti_pembayaran" id="bukti_pembayaran">
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="border: 1px solid; padding:30px; box-shadow:2px 2px 2px; color:#f2f2f2;background:#fff">
            <div class="checkout__order" style="background: #fff;padding:0px">
                <h4 class="order__title">Daftar Barang</h4>
                <div class="checkout__order__products">Subtotal</div>
                <ul class="checkout__total__products">
                    @foreach ($keranjang as $item)
                    <li>{{ $item->produk->nm_produk }} <span>Rp.{{ number_format($item->produk->price * $item->kuantitas, 0, ',', '.') }}</span></li>
                    @endforeach
                </ul>
                <ul class="checkout__total__all">
                    <li>Total <span>{{ number_format($total, 0, ',', '.') }}</span></li>
                </ul>
            
            </div>
        </div>
    </div>
    </form>
   </div>
  </section>
    
@include('dashbord.layouts.footer')

<script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
<script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
<script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
</body>
</html>