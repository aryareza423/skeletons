<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Bootstrap CSS -->
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}"> --}}
    <link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"
/>
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    
  
    
    <title>Toko Online</title>

  </head>
  <body>

    <div class="super_container">
    
      <!-- Header -->
    
      @include('dashbord.layouts.navbar')
  
      <!-- gambar -->
      <div class="container" style="margin-top:200px">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            @foreach ($background as $i => $value)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
            @endforeach

          </ol>
          <div class="carousel-inner">
            @foreach ($background as $i => $value)
            <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
              <img alt="Foto" width="" height="" src="{{ asset('storage/' . $value->gambar) }}" class="d-block w-100" />
            </div>
            @endforeach
         
          </div>
          <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </button>
        </div>
      </div>
      <!--akhir gambar-->

<!--produk-->
      <div class="best_sellers">
        <div class="container">
          {{-- <div class="row">
            <div class="col-12 text-center">
              <div class="section_title new_arrivals_title" style="font-family:ui-monospace">
                <h2>PRODUK TERLARIS</h2>
              </div>
            </div>
          </div> --}}

          <div class="row">
            <div class="col">
              <div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                <section>
                  <div class="container">
                    <div class="row">
                      @foreach ($terjual as $value)
                          <div class="col-lg-12">
                            <div class="product-item men" style="left:230px">
                              <div class="product discount product_filter">
                                <div class="product_image">
                                  <a href="/detail/{{ $value->id }}"><img alt="Foto" width="" height="" src="{{ asset('storage/' . $value->gambar) }}" />
                                  </a>
                                </div>
                                <div class="product_info">
                                  <h6 class="product_name"><a href="/detail/{{ $value->id }}">{{ $value->nm_produk }}</a></h6>
                                  <div class="product_price">Rp. {{ number_format($value->price, 0, ',', '.') }}</div>
                                  <div class="mt-2" style="color: #ffc30d">
                                    {{-- <i class="bi bi-star-fill"></i>  --}}
                                    {{-- <span>{{ $rating }}</span> --}}
                                  </div>
                                  <div style="font-weight: 500 " class="mt-3">Terjual:{{ $value->terjual }} </div>
                                </div>
                              </div>
                              <form action="{{ route('keranjang.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="produk_id" value="{{ $value->id }}">
                                <input type="hidden" name="kuantitas" value="1">
                                <button type="button" class="btn btn-secondary add_to_cart_button " data-toggle="modal" data-target="#modal-ukuran-produk-{{ $value->id }}">
                                  Beli
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="modal-ukuran-produk-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    @csrf
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Pilih Ukuran Sepatu</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                          @foreach ($value->size as $item)
                                          <div class="col-3">
                                            <label class="pilih-ukuran" for="produk-{{ $value->id }}-ukuran-{{ $item->id }}" style="{{ $item->stock == 0 ? 'cursor: not-allowed' : '' }}">
                                              <input type="radio" name="size_id" value="{{ $item->id }}" id="produk-{{ $value->id }}-ukuran-{{ $item->id }}" {{ $item->stock == 0 ? 'disabled' : '' }}>
                                              <div style="border-radius:10px 10px 10px">{{ $item->size }}</div>
                                              <div class="mt-2 text-center">Stok: {{ $item->stock }}</div>
                                            </label>
                                          </div>
                                          @endforeach
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary w-100">Beli</button>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                              </form>
                          </div>
                      </div>
                      @endforeach 
                      </div>
                  </div>
              </section>

        
    
              </div>
            </div>
          </div>
        </div>
      </div>
<!--akhir Produk-->
      <!-- kategori -->
    
      <div class="container mt-3">
        <div class="judul-kategori" style="background-color: #fff; padding: 5px 10px">
          <div class="section_title new_arrivals_title d-block" style="font-family:ui-monospace">
            <h2>KATEGORI</h2>
          </div>
          </div>
          <div class="row text-center row-container mt-5">
            @foreach ($kategori as $value)
            <div class="col-lg-2 col-md-3 col-sm-4 col-6">
              <div class="menu-kategori">
                <a href="/kategori/{{ $value->id }}">  <img alt="Foto" width="130" height="130" src="{{ asset('storage/' . $value->gambar) }}" /></a>
                <p class="mt-2">{{ $value->nm_kategori }}</p>
              </div>
            </div>
            @endforeach
           </div>
         </div>
       </div>
    </div>
    <!--akhir kategori-->
    
      <!-- Rekomendasi -->
      <div class="new_arrivals mb-5">
        <div class="container">
          <div class="row">
            <div class="col text-center">
              <div class="section_title new_arrivals_title" style="font-family:ui-monospace">
                <h2>REKOMENDASI </h2>
              </div>
            </div>
          </div>
        
          <div class="row">
            <div class="col">
              <div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                <section>
                  <div class="container">
                    <div class="row">
                        @foreach ($produk as $value )
                          <div class="col-lg-12">
                            <div class="product-item men" style="left:230px">
                              <div class="product discount product_filter">
                                <div class="product_image">
                                  <a href="/detail/{{ $value->id }}">  <img alt="Foto" width="" height="" src="{{ asset('storage/' . $value->gambar) }}" /></a>
                                </div>
                                <div class="product_info">
                                  <h6 class="product_name"><a href="/detail/{{ $value->id }}">{{ $value->nm_produk }}</a></h6>
                                  <div class="product_price">Rp. {{ number_format($value->price, 0, ',', '.') }}</div>
                                  <div class="mt-2" style="color: #ffc30d">
                                    {{-- <i class="bi bi-star-fill"></i>  --}}
                                    {{-- <span>{{ $rating }}</span> --}}
                                  </div>
                                  <div style="font-weight: 500 " class="mt-3">Terjual:{{ $value->terjual }} </div>
                                </div>
                              </div>
                              <form action="{{ route('keranjang.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="produk_id" value="{{ $value->id }}">
                                <input type="hidden" name="kuantitas" value="1">
                                <button type="button" class="btn btn-secondary add_to_cart_button " data-toggle="modal" data-target="#modal-ukuran-produk-{{ $value->id }}">
                                  Beli
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="modal-ukuran-produk-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                    @csrf
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Pilih Ukuran Sepatu</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                          @foreach ($value->size as $item)
                                          <div class="col-3">
                                            <label class="pilih-ukuran" for="produk-{{ $value->id }}-ukuran-{{ $item->id }}" style="{{ $item->stock == 0 ? 'cursor: not-allowed' : '' }}">
                                              <input type="radio" name="size_id" value="{{ $item->id }}" id="produk-{{ $value->id }}-ukuran-{{ $item->id }}" {{ $item->stock == 0 ? 'disabled' : '' }}>
                                              <div style="border-radius:10px 10px 10px">{{ $item->size }}</div>
                                              <div class="mt-2 text-center">Stok: {{ $item->stock }}</div>
                                            </label>
                                          </div>
                                          @endforeach
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary w-100">Beli</button>
                                      </div>
                                  </div>
                                  </div>
                              </div>
                              </form>
                          </div>
                      </div>
                      @endforeach 
                      </div>
                  </div>
              </section>

        
    
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        {{-- <div class="row">
          <div class="col-3">
            <img src="{{ asset('assets/media/logoproduk/adidas.jpg') }}" width="150" height="150" alt="">
          </div>
          <div class="col-3">
            <img src="{{ asset('assets/media/logoproduk/nike.jpg') }}" width="150" height="150" alt="">

          </div>
        </div> --}}
        {{-- <div class="swiper">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide">  <img src="{{ asset('assets/media/logoproduk/adidas.jpg') }}" width="150" height="150" alt=""></div>
            <div class="swiper-slide">  <img src="{{ asset('assets/media/logoproduk/nike.jpg') }}" width="150" height="150" alt=""></div>
            
          </div>
          <!-- If we need pagination -->
          <div class="swiper-pagination"></div>
        
          <!-- If we need navigation buttons -->
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
        
          <!-- If we need scrollbar -->
          <div class="swiper-scrollbar"></div>
        </div> --}}
        
      </div>
  
      @include('dashbord.layouts.footer')
    
    </div>

    <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
   
    </body>
</html>
