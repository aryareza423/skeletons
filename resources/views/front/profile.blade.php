<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

     {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
     {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}"> --}}
     <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
     <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">

     <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
     {{-- <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" /> --}}

    <title>Profile</title>
  </head>
<body>
    
    @include('dashbord.layouts.navbar')
   
    <form action="/profile/profilesaya" method="POST">
        @csrf
    <div class="container" style="margin-top:15rem">
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="bg-light card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bold m-0">Profile Details</h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
        
            <!--begin::Content-->
            <div id="kt_account_settings_profile_details" class="collapse show">
                <!--begin::Form-->
                <form id="kt_account_profile_details_form" class="form fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate">
                    <!--begin::Card body-->
                
                    <div class="card-body border-top p-9">
                        <!--begin::Input group-->
                      
                        <!--end::Input group-->
        
                        <!--begin::Input group-->
    
                        <div class="row mb-6 p-3">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Nama</label>
                            <!--end::Label-->
        
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row fv-plugins-icon-container">
                                <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama" value="{{ auth()->user()->nama }}" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0">
                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
        
                        <!--begin::Input group-->
                        <div class="row mb-6 p-3">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">Email</label>
                            <!--end::Label-->
        
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row fv-plugins-icon-container">
                                <input type="text" name="email" id="email" autocomplete="off" placeholder="email" value="{{ auth()->user()->email }}" class="form-control form-control-lg form-control-solid" style="cursor:not-allowed" readonly>
                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
        
                        <!--begin::Input group-->
                        <div class="row mb-6 p-3">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label fw-semibold fs-6">
                                <span class="required">No HP</span>
                                {{-- <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" aria-label="Phone number must be active" data-bs-original-title="Phone number must be active" data-kt-initialized="1"></i> --}}
        
                            </label>
                            <!--end::Label-->
                            
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row fv-plugins-icon-container">
                                <input type="tel" name="no_telpon" id="no_telpon" autocomplete="off" placeholder="no_telpon" value="{{ auth()->user()->no_telpon }}"  class="form-control form-control-lg form-control-solid" >
                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
        
                        <!--begin::Input group-->
                        <div class="row mb-6 p-3">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label fw-semibold fs-6">Alamat</label>
                            <!--end::Label-->
        
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text"  name="alamat" id="alamat" autocomplete="off" placeholder="Alamat" value="{{ auth()->user()->alamat }}" class="form-control form-control-lg form-control-solid" >
                            </div>
                            <!--end::Col-->
                        </div>
              
                        <div class="row mb-0">
                            <!--begin::Label-->
                         
        
                            <!--begin::Label-->
                       
                            <!--begin::Label-->
                        </div>
    
                       
                        <!--end::Input group-->
                    </div>
                    <!--end::Card body-->
        
                    <!--begin::Actions-->
                    <div class="card-footer d-flex justify-content-end py-6 px-9 bg-light">
        
                        <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save </button>
                    </div>
                    <!--end::Actions-->
                <input type="hidden"></form>
                <!--end::Form-->
            </div>
            <!--end::Content-->
        </div>
    </div>
</form>
    @include('dashbord.layouts.footer')

<script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
<script src="{{ asset('assets/jssepatu/custom.js') }}"></script>


<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
</body>

