<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF </title>
</head>
<body>
  <table style="width: 100%">
    <tr>
        <td style="width: 50%; font-size:2rem; ">INVOICE</td>
    </tr>
    <tr>
        <td style="width:50%;text-align:right"><img src="{{ public_path('assets/media/logoproduk/logoskeletons.png') }}" alt="" style="width: 150px;">
        </td>
    </tr>
    <tr>
        <td style="font-size: 1.5rem;text-align:right">SKELETONS</td>
    </tr>
  </table>
  <table style="width:100%;margin-top:2rem">
 <tr>
    <th style="border:1px solid rgb(112, 112, 112); "></th>
 </tr>
  </table>
  <table style="width: 100%; margin-top:3rem; borde:12px">
    <tr>
        <th style="text-align: left">Dipesan Kepada</th>
        <th style="text-align: left">Alamat</th>
        <th style="text-align: left">Info Pembayaran</th>
    </tr>
    <tr>
        <td>Nama : {{ $checkout->pengiriman->nama }} </td>
        <td>Alamat :{{ $checkout->pengiriman->alamat }}</td>
        <td>Total Tagihan</td>
    </tr>
    <tr>
        <td>No Hp : {{ $checkout->pengiriman->no_hp }}</td>
        <td>Provinsi :{{ $checkout->pengiriman->provinsi->nm_provinsi }}</td>
        <td>{{number_format( intval(str_replace(',', '',  $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</td>
        
    </tr>
        <tr>
            <td>Nama Penerima : {{ $checkout->pengiriman->nm_penerima }}</td>
            <td>Kota : {{ $checkout->pengiriman->kota->nm_kota }}</td>
        </tr>
        <tr>
            <td>Jenis Pengiriman :{{ $checkout->pengiriman->jenis_kurir }}</td>
            <td>Kode Pos : {{ $checkout->pengiriman->kode_pos }}</td>
        </tr>
        <tr>
            <td>Jenis Pembayaran : {{ $checkout->bank->nm_bank }}</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
        </tr>
  </table>
  <table class="produk" style="width:100%; margin-top:3rem;border:1px solid black;border-collapse: collapse; ">
    
    <tr>
      <th style="text-align: center; border:1px solid black;">Produk</th>
      <th style="text-align: center; border:1px solid black ">Kuantitas </th>
      <th style=" border:1px solid black">Total</th>
    </tr>
    @foreach ($checkout->pesanans as $item)
    <tr>
      <td style="border:1px solid black">
        <div style="margin-top:1rem;margin-bottom:1rem; margin-left:2rem">{{ $item->produk->nm_produk}}-{{ $item->sizes->size }}</div>
    </td>
      <td style="text-align: center;border:1px solid black">  {{ $item->kuantitas }}</td>
      <td style="text-align: center;border:1px solid black">  Rp.{{ number_format($item->produk->price * $item->kuantitas, 0, ',', '.') }}</td>
    </tr>
 @endforeach     
    <tr>
        <th style="border:1px solid black"></th>
        <th style="border:1px solid black"></th>
        <th style="border:1px solid black">Ongkir : Rp.{{ number_format( $checkout->pengiriman->harga_ongkir, 0, ',', '.') }}</th>
    </tr>
    <tr>
        <th style="border:1px solid black"></th>
        <th style="border:1px solid black"></th>
        <th >Total : Rp.{{number_format( intval(str_replace(',', '.', $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</th>
    </tr>
   
  </table>
</body>
</html>