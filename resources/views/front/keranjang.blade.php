<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap"
    rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <title>Keranjang</title>
</head>
<body>
  <!-- Page Preloder -->
  <div id="preloder">
      <div class="loader"></div>
  </div>

  <!-- Offcanvas Menu Begin -->
  <div class="offcanvas-menu-overlay"></div>
  @include('dashbord.layouts.navbar')
  
  <!-- Header Section End -->

  <!-- Breadcrumb Section Begin -->
  <section class="breadcrumb-option">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="breadcrumb__text">
                      <div class="breadcrumb__links">
                          <span></span>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- Breadcrumb Section End -->

  <!-- Shopping Cart Section Begin -->
  <section class="shopping-cart spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-8">
                  <div class="shopping__cart__table">
                      <table>
                          <thead>
                              <tr>
                                  <th>Product</th>
                                  <th>Quantity</th>
                                  <th>Harga</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                @if (count($keranjang))
                                @foreach ($keranjang as $item)
                                <td class="product__cart__item">
                                    <div class="product__cart__item__pic">
                                        <img src="{{ asset('storage/' . $item->produk->gambar) }}" alt="">
                                        <h6 class="mt-3">{{ $item->produk->nm_produk }}-{{ $item->size->size }}</h6>
                                    </div>
                                </td>
                                <form action="{{ url('/keranjang/'. $item->id) }}" method="POST"  enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="_method" value="PUT">
                                    <td class="quantity__item">
                                        <div class="quantity">
                                            <div class="">
                                                <input type="number" value="{{ $item->kuantitas }}" class="input-text kuantitas text-center" max="{{ $item->size->stock }}" data-id="{{ $item->id }}" name="kuantitas" min="0" autocomplete="off">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="cart__price">{{ number_format($item->produk->price , 0, ',', '.') }}</td>
                                    <td class="">
                                        {{-- <button class="btn btn-primary m-2" type="submit"><i class="bi bi-arrow-counterclockwise"></i> Update</button> --}}
                                    </td>
                                </form>
                                <form action="{{ url('/keranjang/'. $item->id) }}" method="POST" enctype="multipart/form-data">
                                 @csrf
                                 @method('delete')
                                <td>
                                    <button class="btn btn-danger" type="submit"><i class="bi bi-trash"></i> Hapus</button>
                                </td>
                            </form>
                            </tr>
                            @endforeach

                            @else
                            <tr>
                                <td colspan=3 class="text-center">
                                    <h6>Keranjang Kosong</h6>
                                </td>
                            </tr>
                            @endif

                          </tbody>
                            <tr>
                      </table>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="cart__total">
                      <h6>Total Produk</h6>
                    
                      <ul>
                        <li>Subtotal Produk :<span></span></li>
                        @foreach ($keranjang as $item)
                        <li>{{ $item->produk->nm_produk }}-{{ $item->size->size }} <span>Rp.{{ number_format($item->produk->price * $item->kuantitas , 0, ',', '.') }}</span></li>
                        @endforeach
                        <li>Total <span>Rp.{{ number_format($total ,0, ',', '.') }}</span></li>
                    </ul>
                      
                      <a href="/checkout" class="primary-btn">Chekout</a>
                      <a href="/" class="primary-btn mt-3">Kembali Belanja</a>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- Shopping Cart Section End -->

  <!-- Footer Section Begin -->
  @include('dashbord.layouts.footer')
  <script>
      function debounce(func, timeout = 1000) {
            let timer;
            return (...args) => {
                clearTimeout(timer);
                timer = setTimeout(() => {
                    func.apply(this, args);
                }, timeout);
            };
        }

        document.querySelectorAll('.kuantitas').forEach(item => {
            item.addEventListener('input', debounce(function() {
                const id = item.dataset.id;
                const kuantitas = item.value;
                const url = `/keranjang/${id}`;
                $.ajax({
                    url: url,
                    method: 'post',
                    headers: {
                        'X-CSRF-TOKEN': '{{ @csrf_token() }}'
                    },
                    data: {
                        kuantitas: kuantitas,
                    },
                    success: function(response) {
                        window.location.reload()
                    }
                });
            }, 1000));
        });
  </script>
 
  

  <!-- Js Plugins -->
  <script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
  <script src="{{ asset('assets/js/mixitup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
</body>
</html>