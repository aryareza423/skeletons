<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}"> --}}
    
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <title>Konfirmasi</title>
</head>
<body>
  <!--navbar-->
  @include('dashbord.layouts.navbar')
  <!--akhir Navbar-->
<!--pesananan-->
<section class="breadcrumb-option">
  <div class="container">
      <div class="row">
          <div class="col-lg-12">
              <div class="breadcrumb__text">
                  <div class="breadcrumb__links">
                      <span></span>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
<section class="checkout spad">
  <div class="container">
      <div class="checkout__form">
          <form method="POST" action="/pembayaran">
            <input type="hidden" name="nama" value="{{ request()->nama }}">
            <input type="hidden" name="no_hp" value="{{ request()->no_hp }}">
            <input type="hidden" name="alamat" value="{{ request()->alamat }}">
            <input type="hidden" name="provinsi_id" value="{{ request()->provinsi_id }}">
            <input type="hidden" name="kota_id" value="{{ request()->kota_id }}">
            <input type="hidden" name="bank_id" value="{{ request()->bank_id }}">
            <input type="hidden" name="kode_pos" value="{{ request()->kode_pos }}">
            <input type="hidden" name="courier" value="{{ request()->courier }}">
            <input type="hidden" name="layanan" value="{{ request()->layanan }}">
            <input type="hidden" name="nm_penerima" value="{{ request()->nm_penerima }}">
            


            @csrf
              <div class="row">
                  <div class="col-lg-4 col-md-6 mt-3">
                      <h6 class="checkout__title">Daftar Alamat</h6>
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="checkout__input">
                                  <h5 style="font-family: mate">Nama : {{ request()->nama }}</h5>
                              </div>
                          </div>
                          <div class="col-lg-12 mt-2">
                              <div class="checkout__input">
                                <h5 style="font-family: mate">No Hp : {{ request()->no_hp }}</h5>
                              </div>
                          </div>
                          <div class="col-lg-12 mt-2">
                              <div class="checkout__input">
                               <h5 style="font-family: mate">Alamat : {{ request()->alamat }}</h5>
                              </div>
                          </div>
                      </div>
                    
                      <div class="row">
                          <div class="col-lg-12 mt-2">
                              <div class="checkout__input">
                                 <h5 style="font-family: mate">Provinsi : {{ $provinsi->nm_provinsi }}</h5>
                              </div>
                          </div>
                          <div class="col-lg-12 mt-2">
                              <div class="checkout__input">
                                <h5 style="font-family: mate">Kota : {{ $kota->nm_kota }}</h5>
                              </div>
                          </div>
                          <div class="col-lg-12 mt-2">
                            <div class="checkout__input">
                              <h5 style="font-family: mate">Kode pos : {{ request()->kode_pos}}</h5>
                            </div>
                        </div>
                      </div> 
                      <div class="row">
                        <div class="col-lg-12 mt-2">
                          <div class="checkout__input">
                            <h5 style="font-family: mate">Nama Penerima : {{ request()->nm_penerima }}</h5>
                          </div>
                      </div>
                      </div>
                  </div>


                  <!--pengiriman-->
                  <div class="col-lg-4 col-md-6 mt-3">
                    <h6 class="checkout__title">Pengiriman</h6>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="checkout__input">
                                <h5 style="font-family: mate">Jenis Pengiriman : {{ request()->courier }}</h5>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-2">
                            <div class="checkout__input">
                              <h5 style="font-family: mate">Layanan (ongkir) : Rp. {{ number_format( request()->layanan, 0, ',', '.') }}</h5>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-2">
                            <div class="checkout__input">
                              <h5 style="font-family: mate">jenis pembayaran Bank:  {{ $bank->nm_bank }}</h5>
                              <img src="{{ asset('storage/' . $bank->gambar) }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>


                <!--detail Pesanan-->
                <div class="col-lg-4 col-md-6">
                  <div class="checkout__order">
                      <h4 class="order__title">Pesanan Anda</h4>
                      <div class="checkout__order__products">Subtotal</div>
                      <ul class="checkout__total__products">
                          @foreach ($keranjang as $item)
                          <li>{{ $item->produk->nm_produk }}-{{ $item->size->size }} <span>Rp.{{ number_format($item->produk->price * $item->kuantitas, 0, ',', '.') }}</span></li>
                          @endforeach
                          <li>Ongkir <span>Rp.{{ number_format( request()->layanan, 0, ',', '.') }}</span></li>
                      </ul>
                      <ul class="checkout__total__all">
                          <li>Total <span>{{number_format( intval(str_replace(',', '', $total)) + request()->layanan) }}</span></li>
                      </ul>
                      <a href="/" class="site-btn bg-danger text-center" style="border-radius:0">Batalkan Pesanan</a>
                      <button type="submit" class="site-btn">Lanjutkan Pembayaran</button>
                  </div>
              </div>
              </div>
          </form>
      </div>
  </div>
</section>
<!--akhirpesanan-->
@include('dashbord.layouts.footer')

  <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
  <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
  <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
  <script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
  <script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>

</body>
</html>