<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
      {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}"> --}}
      <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/kategori.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/kategoriresponsive.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
    <title>Kategori</title>
  </head>
  <body>

    <div class="super_container">
    
      <!-- Header -->
      @include('dashbord.layouts.navbar')
      <div class="container product_section_container">
        <div class="row">
          <div class="col product_section clearfix">
    
            <!-- Breadcrumbs -->
            <div class="row">
            <div class="breadcrumbs d-flex flex-row align-items-center mt-5">
          
            <div class="col-4">
              <h4>Kategori </h4>
            </div>
            <div class="col-4">
              <div class="dropdown">
                <button class="btn btn-transparant dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Harga 
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="?sort=terendah">Terendah</a>
                  <a class="dropdown-item" href="?sort=tertinggi">Tertinggi</a>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="container-fluid">
                <form action="{{ url('/kategori/'.$id) }}" method="GET" class="d-flex ">
                  <input class="form-control me-2" type="search" placeholder="Cari Produk" aria-label="Search" style="border-radius:10px 10px 10px;" name="keyword" autocomplete="off">
                  <button class="btn btn-outline-primary" type="submit" style="border-radius:10px 10px 10px; margin-left:10px">Cari</button>
                </form>
              </div>
            </div>
           </div>
            
              
              
              
             
                
                
             
            </div>
    
            <!-- Sidebar -->
            <div class="sidebar">
              <div class="sidebar_section">
                <div class="sidebar_title">
                  <h5>Produk Kategori</h5>
                  
                </div>
             
                <ul class="sidebar_categories">
                  @foreach ($kategori as $value)
                  <li><a href="/kategori/{{ $value->id }}">{{ $value->nm_kategori }}</a></li>
                  @endforeach
                </ul>
              </div>
    
              <!-- Price Range Filtering -->
              <!-- Sizes -->
              <div class="sidebar_section">
                <div class="sidebar_title">
                  <h5>Produk</h5>
                </div>
                <ul class="sidebar_categories">
                  <li><a href="?sort=terbaru">Terbaru</a></li>
                  {{-- <li><a href="#">Berdasarkan Rating</a></li> --}}
                </ul>
              </div>
    
              <!-- Color -->
           
    
            </div>
    
            <!-- Main Content -->
    
            <div class="main_content">
    
              <!-- Products -->
    
              <div class="products_iso">
                <div class="row">
                  <div class="col">
                    <div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
          
                      <section class="product spad">
                        <div class="container">
                          <div class="row">
                              @foreach ($produk as $value )
                                <div class="col-lg-12">
                                  <div class="product-item men" style="left:230px">
                                    <div class="product discount product_filter">
                                      <div class="product_image">
                                        <a href="/detail/{{ $value->id }}">  <img alt="Foto" width="" height="" src="{{ asset('storage/' . $value->gambar) }}" /></a>
                                      </div>
                                      <div class="product_info">
                                        <h6 class="product_name"><a href="/detail/{{ $value->id }}">{{ $value->nm_produk }}</a></h6>
                                        <div class="product_price">Rp. {{ number_format($value->price, 0, ',', '.') }}</div>
                                        <div class="mt-2" style="color: #ffc30d">
                                          {{-- <i class="bi bi-star-fill"></i>  --}}
                                          {{-- <span>{{ $rating }}</span> --}}
                                        </div>
                                        <div style="font-weight: 500 " class="mt-3">Terjual:{{ $value->terjual }} </div>
                                      </div>
                                    </div>
                                    <form action="{{ route('keranjang.store') }}" method="POST" enctype="multipart/form-data">
                                      @csrf
                                      <input type="hidden" name="produk_id" value="{{ $value->id }}">
                                      <input type="hidden" name="kuantitas" value="1">
                                      <button type="button" class="btn btn-secondary add_to_cart_button " data-toggle="modal" data-target="#modal-ukuran-produk-{{ $value->id }}">
                                        Beli
                                      </button>
                                      <!-- Modal -->
                                      <div class="modal fade" id="modal-ukuran-produk-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                          @csrf
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLongTitle">Pilih Ukuran Sepatu</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <div class="row">
                                                @foreach ($value->size as $item)
                                                <div class="col-3">
                                                  <label class="pilih-ukuran" for="produk-{{ $value->id }}-ukuran-{{ $item->id }}" style="{{ $item->stock == 0 ? 'cursor: not-allowed' : '' }}">
                                                    <input type="radio" name="size_id" value="{{ $item->id }}" id="produk-{{ $value->id }}-ukuran-{{ $item->id }}" {{ $item->stock == 0 ? 'disabled' : '' }}>
                                                    <div style="border-radius:10px 10px 10px">{{ $item->size }}</div>
                                                    <div class="mt-2 text-center">Stok: {{ $item->stock }}</div>
                                                  </label>
                                                </div>
                                                @endforeach
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-primary w-100">Beli</button>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            @endforeach 
                            </div>
                        </div>
                    </section>
          
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      <!-- Benefit -->
      {{-- @include('dashbord.layouts.footer') --}}
  
  
    
    
    </div>
    
    <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    </body>
</html>

