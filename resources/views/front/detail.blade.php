<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
      {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}"> --}}
      <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/responsive.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/detail.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/front/detailresponsive.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/animate.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/csscoba/style.css') }}">

      {{-- <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}"> --}}

      <style>
  .progress {

  padding:0;
  width:50%;
  overflow:hidden;
  background:#e5e5e5;
  border-radius:6px;
}

.bar {
	position:relative;
  float:left;
  height:100%;
  background: #ffc30d;
}

.percent {
	position:absolute;
  top:50%;
  left:50%;
  transform:translate(-50%,-50%);
  margin:0;
  font-family:tahoma,arial,helvetica;
  font-size:12px;
  color:white;
}

.spesifiasi-produk table td{
  padding: 0.75rem 1.25rem
}
      </style>


    <title>Detail Produk</title>
  </head>
  <body>

    <div class="super_container">
    
      <!-- Header -->
    
      @include('dashbord.layouts.navbar')

      <div class="container single_product_container">
        <div class="row">
          <div class="col">
            <div class="breadcrumbs d-flex flex-row align-items-center">
             <h5>Detail Produk</h5>
            </div>
          </div>
        </div>
    
        <div class="row mb-5">
          <div class="col-lg-7">
            <div class="single_product_pics">
              <div class="row">
                <div class="col-lg-3 thumbnails_col order-lg-1 order-2">
                  <div class="single_product_thumbnails">
                    <ul>
                      @foreach ($produk->spesifikasiproduks->gambarspesifikasi as $value )
                      <li class="active"> <img  src="{{ asset('storage/' . $value->gambar) }}" alt="" data-image="{{ asset('storage/' . $value->gambar) }}" /></li>
                      @endforeach
                    </ul>
                  </div>
                </div>
                <div class="col-lg-9 image_col order-lg-2 order-1">
                  <div class="single_product_image">
                    <div class="single_product_image_background" style="background-image:url(images/single_2.jpg);"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="product_details p-3 mt-5">
              <div class="product_details_title mb-3"> 
                <h2>{{ $produk->nm_produk }}</h2>
              </div>
              <div class="mt-2 d-flex" style="color: #ffc30d">
                <h1 class="d-flex" style="font-size:1.25rem"><span class="">{{ round((float)$rating, 1) }}</span> <span >/5</span></h1> 
                  <i class="bi bi-star-fill" style="margin-left:0.75rem"></i> 
                  <i class="bi bi-star-fill"></i>
                  <i class="bi bi-star-fill"></i>
                  <i class="bi bi-star-fill"></i>
                  <i class="bi bi-star-fill"></i>
                  <span style="margin-left:0.75rem; color:#ccc9c9">|</span>
                  <span style="font-weight: 500;margin-left:0.75rem;color:black">{{ $produk->terjual }} Terjual  </span>
                </div>
              <div class="product_price mt-3">Rp.{{ number_format($produk->price, 0, ',', '.') }}</div>
              <form action="{{ route('keranjang.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                <input type="hidden" name="kuantitas" value="1">
             <!-- Button trigger modal -->
            <button type="button" class="btn btn-outline-danger w-50 mt-5" data-toggle="modal" data-target="#modal-ukuran-produk-{{ $produk->id }}" >
              Beli
            </button>

            <!-- Modal -->
              <div class="modal fade" id="modal-ukuran-produk-{{ $produk->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Pilih Ukuran Sepatu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                  
                    <div class="modal-body">
                      <div class="row">
                        @foreach ($produk->size as $item)
                        <div class="col-3">
                          <label class="pilih-ukuran" for="produk-{{ $value->id }}-ukuran-{{ $item->id }}" style="{{ $item->stock == 0 ? 'cursor: not-allowed' : '' }}">
                            <input type="radio" name="size_id" value="{{ $item->id }}" id="produk-{{ $value->id }}-ukuran-{{ $item->id }}" {{ $item->stock == 0 ? 'disabled' : '' }}>
                            <div style="border-radius:10px 10px 10px">{{ $item->size }}</div>
                            <div class="mt-2 text-center">Stok: {{ $item->stock }}</div>
                          </label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-outline-danger w-100">Beli</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      <div class="row">
        <div class="col-12 mt-5">
          <div class="spesifiasi-produk">
            <h6 style="font-weight:500; font-size:1.25rem; padding:0.75rem 1.25rem; background:#f2eeef;">Spesifikasi Produk</h6>
            <table class="mb-5 mt-1">
              <tbody>
                <tr>
                  <td>MEREK</td>
                  <td>{{ $produk->spesifikasiproduks->merek }}</td>
                </tr>
                <tr>
                  <td>Model Sepatu</td>
                  <td>{{ $produk->spesifikasiproduks->model_sepatu }}</td>
                </tr>
                <tr>
                  <td>Negara Asal</td>
                  <td>{{ $produk->spesifikasiproduks->negara_asal }}</td>
                </tr>
               
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mt-2 mb-5">
          <div class="spesifiasi-produk">
            <h6 style="font-weight:500; font-size:1.25rem; padding:0.75rem 1.25rem; background:#f2eeef;">Deskripsi Produk</h6>
           <article style="white-space:pre-line; line-height:1.8">
          {{ $produk->deskripsi }}
          </article>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mt-5">
          <div class="spesifiasi-produk">
            <h6 style="font-weight:500; font-size:1.25rem; padding:0.75rem 1.25rem; background:#f2eeef;">Penilaian Produk</h6>
        </div>
        <div class="row">
        <div class="col-4 mb-5 mt-3">
          <h1 class="d-flex"><span class="">{{ round((float)$rating, 1) }}</span> <span >/5</span></h1> 
          <div class="mt-2 fa-2x d-flex" style="color: #ffc30d">
            <i class="bi bi-star-fill"></i> 
            <i class="bi bi-star-fill"></i>
            <i class="bi bi-star-fill"></i>
            <i class="bi bi-star-fill"></i>
            <i class="bi bi-star-fill"></i>
          </div>
        </div>
      
          <div class="col-lg-8 mt-3">
            <div class="progress">
              <div class="bar" style="width: {{ $banyak_rating > 0 ? ($rating1 / $banyak_rating * 100) : 0 }}%">
                <p class="percent">{{ $banyak_rating > 0 ? ($rating1 / $banyak_rating * 100) : 0 }}%</p>
              </div>
            </div>
          
            <div class="progress mt-1">
              <div class="bar"style="width: {{ $banyak_rating > 0 ? ($rating2 / $banyak_rating * 100) : 0 }}%">
                <p class="percent">{{ $banyak_rating > 0 ? ($rating2 / $banyak_rating * 100) : 0 }}%</p>
              </div>
            </div>
            <div class="progress mt-1">
              <div class="bar" style="width :{{ $banyak_rating > 0 ? ($rating3 / $banyak_rating * 100) : 0 }}%">
                <p class="percent">{{ $banyak_rating > 0 ? ($rating3 / $banyak_rating * 100) : 0 }}%</p>
              </div>
            </div>
            <div class="progress mt-1">
              <div class="bar" style="width: {{ $banyak_rating > 0 ? ($rating4 / $banyak_rating * 100) : 0 }}%">
                <p class="percent">{{ $banyak_rating > 0 ? ($rating4 / $banyak_rating * 100) : 0 }}%</p>
              </div>
            </div>
            <div class="progress mt-1">
              <div class="bar" style="width: {{ $banyak_rating > 0 ? ($rating5 / $banyak_rating * 100) : 0 }}%">
                <p class="percent">{{ $banyak_rating > 0 ? ($rating5 / $banyak_rating * 100) : 0 }}%</p>
              </div>
            </div>
           </div>
      </div>
        </div>
        <div class="row">
        @foreach ($ratingKomen as $item)
          <div class="col-md-12 mt-3">
            <div class="media mb-4">
              <!-- <img src="img/user.jpg" alt="Image" class="img-fluid mr-3 mt-1" style="width: 45px" /> -->
              <div class="media-body">
                <h5 class="m-1" style="font-weight:700; font-size:1.25rem">{{ $item->user->nama }}</h5>
                <h6 class="mt-2" style="font-size:small">{{ $item->produk->nm_produk }}</h6>
                <div class="text-primary mb-2 d-flex">
                  @for ($i = 0; $i < $item->rating; $i++)
                  <div class="mt-2" style="color: #ffc30d; font-size:1.5rem">
                    <i class="bi bi-star-fill"></i> 
                  </div>
              @endfor
                </div>
                <p style="font-weight:500">{{ $item->komen }}</p>
                
                @if ($item->gambar)
                  <img alt="gambar" width="" height="" class="w-25" src="{{ asset('storage/' . $item->gambar) }}" />
                @endif
            
              
              </div>
            </div>
          </div>
          @endforeach
        </div>
        </div>
          </div>
      </div>
            
         
         </div>
      </div>
      @include('dashbord.layouts.footer')
    </div>
    <script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
    <script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
    <script src="{{ asset('assets/jssepatu/single_custom.js') }}"></script>
    <script src="{{ asset('assets/jscoba/main.js') }}"></script>

     
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    </body>
</html>
