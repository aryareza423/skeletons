<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
 
    <link rel="stylesheet" href="{{ asset('assets/css/front/main.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/stylessepatu/bootstrap4/bootstrap.min.css') }}">
    <link href="{{ asset('assets/fontawesome/css/all.min.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('assets/csssepatubaru/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/csscoba/style.css') }}">

    <title>Pesanan</title>
    <style>
      table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
    </style>
</head>
<body>
  @include('dashbord.layouts.navbar')
  <section class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__text">
                    <div class="breadcrumb__links">
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <section class="checkout spad">
    <div class="container">
    <div class="breadcrumbs d-flex flex-row align-items-center">
        <ul class="m-4">
        
        </ul>
      </div>
      <div class="row px-xl-5">
        <div class="col">
          <div class="bg-light p-30">
            <div class="nav nav-tabs mb-4">
              <a class="nav-item nav-link text-dark bg-transparent p-3" data-toggle="tab" href="#tab-pane-1" style="border-radius:10px 10px 10px; margin:0px">Pesanan Saya</a>
              <a class="nav-item nav-link text-dark bg-transparent p-3" data-toggle="tab" href="#tab-pane-2"  style="border-radius:10px 10px 10px; margin:0px">Riwayat Pesanan</a>


            </div>
            <div class="tab-content">
              <div class="tab-pane fade" id="tab-pane-1">
                <h4 class="mb-3">Pesanan Saya</h4>
                <table class="w-100">

                  <tr>
                    <th >Nama</th>
                    <th>Alamat</th>
                    <th>Kode pos</th>
                    <th>nama penerima</th>
                    <th>total harga</th>
                    <th>status</th>
                    <th></th>
                    <th>Action</th>

                  </tr>
                   @foreach ($checkout as $item)
                  <tr>
                    <td>{{ $item->pengiriman->nama }}</td>
                    <td>{{ $item->pengiriman->alamat }}</td>
                    <td> {{ $item->pengiriman->kode_pos }}</td>
                    <td>{{ $item->pengiriman->nm_penerima }}</td>
                    <td>{{number_format( intval(str_replace(',', '', $item->total_harga)) + $item->pengiriman->harga_ongkir) }}</td>
                    <td>  
                      @if ($item->status == 'Pesanan Baru')
                      <span class="badge badge-warning">Menunggu Pembayaran</span>
                      @elseif ($item->status == 'Sedang DiProses')
                      <span class="badge badge-primary">Sudah Dikonfirmasi, Sedang DiProses Pesanan Anda</span>
                      @elseif ($item->status == 'Dalam Pengiriman')
                      <span class="badge badge-primary">Dalam Pengiriman</span>
                      @elseif ($item->status == 'Telah Selesai')
                      <span class="badge badge-success">Selesai</span>
                      @endif
                    </td>
                    @if ($item->status == 'Dalam Pengiriman')
                    <td>
                      <form action="{{ url('/user/selesai/'. $item->id. '/updatestatusselesai') }}" method="POST">
                      @csrf
                      <input type="hidden" name="status" value="Telah Selesai">
                      <button class="badge badge-success" type="submit">Pesanan Diterima</button>
                      </form>
                    </td>
                    @endif
                    <td> <a href="/pembayaran/{{ $item->id }}"><i class="fa-solid fa-eye" style="color:black"></i></a></td>
                    
                  </tr>
                  @endforeach
                
                </table>
              </div>
              <div class="tab-pane fade" id="tab-pane-2">
                <div class="row">
                  <div class="col-md-12">
                    <h4 class="mb-4">Riwayat Pesanan</h4>
                    <div class="media mb-4">
                      <table class="w-100">
                        <tr>
                          <th >Nama</th>
                          <th>Alamat</th>
                          <th>Kode pos</th>
                          <th>nama penerima</th>
                          <th>total harga</th>
                          <th>status</th>
                          <th>Action</th>

                        </tr>
                        <tr>
                          @if (count($selesai))
                          @foreach ($selesai as $item)
                          <tr>
                            <td>{{ $item->pengiriman->nama }}</td>
                            <td>{{ $item->pengiriman->alamat }}</td>
                            <td> {{ $item->pengiriman->kode_pos }}</td>
                            <td>{{ $item->pengiriman->nm_penerima }}</td>
                            <td>{{number_format( intval(str_replace(',', '', $item->total_harga)) + $item->pengiriman->harga_ongkir) }}</td>
                            <td>  
                              @if ($item->status == 'Telah Selesai')
                              <span class="badge badge-success">Selesai</span>
                              @endif
                            </td>
                            <td> <a href="/pembayaran/{{ $item->id }}"><i class="fa-solid fa-eye" style="color:black"></i></a></td>
                            
                        </tr>
                       @endforeach
                       @else
                       <tr>
                           <td colspan=7 class="text-center">
                               <h6>Tidak Ada Riwayat Pesanan</h6>
                           </td>
                       </tr>
                       @endif
                      </table>
                      <div class="media-body">
                        <h6></h6>
                        <div class="text-primary mb-2">
                     
                        </div>
                       <p></p>
                      </div>
                    </div>
                    
                  </div>
               
                </div>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    
{{-- @include('dashbord.layouts.footer') --}}
<script src="{{ asset('assets/jssepatu/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4/popper.js') }}"></script>
<script src="{{ asset('assets/stylessepatu/jbootstrap4//bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/pluginssepatu/easing/easing.js') }}"></script>
<script src="{{ asset('assets/jssepatu/custom.js') }}"></script>
<script src="{{ asset('assets/jssepatu/categories_custom.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/jquery.slicknav.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/mixitup.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/jssepatubaru/main.js') }}"></script>
</body>
</html>