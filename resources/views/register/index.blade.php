<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <title>Toko Online</title>
  </head>
  <body>
    <div class="container d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
      <!--begin::Form-->
      
      <div class="d-flex flex-center flex-column flex-lg-row-fluid">
        <form action="/register" method="post" class="form-container">
          @csrf
        <div class="w-lg-500px p-10">
            <div class="text-center mb-11">  
              <h1 class="text-dark fw-bolder mb-3">Daftar</h1>
              <div class="text-gray-500 fw-semibold fs-6">Silakan Daftar</div>
            </div>
            <div class="separator separator-content my-14">
              <span class="w-125px text-gray-500 fw-semibold fs-7">Isi Data Diri Anda!!</span>
            </div>
            <div class="fv-row mb-8 fv-plugins-icon-container">
              <!--begin::Email-->
             
              <div class="position-relative mb-5">
              <input type="text"  class="form-control "  @error('name') is-invalid @enderror" name="nama" autocomplete="off" id="nama" aria-describedby="emailHelp" placeholder="Nama" required value="{{ old('nama') }}" />
              @error('nama')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
              </div>
              <div class="mb-5">
              <input type="email" class="form-control bg-transparent"  @error('email')is-invalid @enderror" name="email" id="exampleInputEmail1" autocomplete="off" aria-describedby="emailHelp" placeholder="email"  required value="{{ old('email') }}" />
              @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
              <!--end::Email-->
            <div class="fv-plugins-message-container invalid-feedback"></div></div>
            <!--begin::Input group-->
            <div class="fv-row mb-8 fv-plugins-icon-container" data-kt-password-meter="true">
              <!--begin::Wrapper-->
              <div class="mb-5">
                <!--begin::Input wrapper-->
                <div class="position-relative mb-3">
                  <input type="password" class="form-control  @error('password')is-invalid @enderror" name="password" id="password" autocomplete="off" placeholder="password" required value="{{ old('password') }}" />
                  <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                  </span>
                  @error('password')
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                  @enderror
                </div>
              </div>
              <div class="mb-5">
              <input type="number"  class="form-control "  @error('no_telpon')is-invalid @enderror" name="no_telpon" autocomplete="off" id="no_telpon" placeholder="No Telpon" required value="{{ old('no_telpon') }}" />
              @error('no_telpon')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
              <div class="mb-5">
              <input type="text"  class="form-control"  @error('alamat')is-invalid @enderror" name="alamat" autocomplete="off" id="alamat" placeholder="ALAMAT" required value="{{ old('alamat') }}" />
              @error('alamat')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            </div>
            <div class="d-grid mb-10">
              <button type="submit"class="btn btn-primary">
                <span class="indicator-label">Daftar</span>
                <span class="indicator-progress">Please wait... 
                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                <!--end::Indicator progress-->
              </button>
            </div>
            <div class="text-gray-500 text-center fw-semibold fs-6">SUdah Punya Akun ?
            <a href="/login" class="link-primary fw-semibold">Login</a></div>
          </div>
        </div>
      </div>
    </form>
  

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/widgets.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
  </body>
</html>