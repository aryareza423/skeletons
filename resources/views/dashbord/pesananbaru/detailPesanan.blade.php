@extends('dashbord.layouts.main')

@section('container')
<div id="kt_app_content" class="app-content  flex-column-fluid ">
    <!--begin::Content container-->
<div id="kt_app_content_container" class="app-container  container-xxl ">
    
<!-- begin::Invoice 3-->
<div class="card">
<!-- begin::Body-->
<div class="card-body py-20">
 
<!-- begin::Wrapper-->
<div class="mw-lg-950px mx-auto w-100">
    <!-- begin::Header-->
    
    <div class="d-flex justify-content-between flex-column flex-sm-row mb-19">
        <h4 class="fw-bolder text-gray-800 fs-2qx pe-5 pb-7">INVOICE</h4>

        <!--end::Logo-->
        <div class="text-sm-end">
            <!--begin::Logo-->
            <div class="d-block mw-150px ms-sm-auto">
                <img alt="Logo" src="{{ asset('assets/media/logoproduk/logoskeletons.png') }}" class="w-100">
                <span class="me-10" style="font-size:1.5rem; font-family: fantasy;">SKELETONS</span>
            </div>
            <!--end::Logo-->

            <!--begin::Text-->
           
            <!--end::Text-->
        </div>
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="pb-12">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column gap-7 gap-md-10">
            <!--begin::Message-->
          
            <!--begin::Message-->

            <!--begin::Separator-->
            <div class="separator"></div>
            

            <div class="d-flex flex-column flex-sm-row gap-7 gap-md-10 fw-bold">
                <div class="flex-root d-flex flex-column">
                   
                    <span class="text-muted">DiPesan Kepada</span>
                    
                    <span class="fs-5 mt-3">Nama :  {{ $checkout->pengiriman->nama }}</span>
                    <span class="fs-5 ">No Hp :  {{ $checkout->pengiriman->no_hp }}</span>
                    <span class="fs-5 ">Nama Penerima   :  {{ $checkout->pengiriman->nm_penerima }}</span>
                    <span class="fs-5 ">Jenis Pengiriman  : {{ $checkout->pengiriman->jenis_kurir }}</span>
                    <span class="fs-5 ">Jenis Pembayaran :  {{ $checkout->bank->nm_bank }}</span>
                    <img src="{{ asset('storage/' . $checkout->bank->gambar) }}" alt="" class="w-50">

                </div>

             

                <div class="flex-root d-flex flex-column">
                    <span class="text-muted ">Alamat</span>
                    <span class="fs-5 mt-3">Alamat : {{ $checkout->pengiriman->alamat }}</span>
                    <span class="fs-5">Provinsi : {{ $checkout->pengiriman->provinsi->nm_provinsi }}</span>
                    <span class="fs-5">Kota : {{ $checkout->pengiriman->kota->nm_kota }}</span>
                    <span class="fs-5">Kode Pos : {{ $checkout->pengiriman->kode_pos }}</span>


                </div>

                <div class="flex-root d-flex flex-column">
                    <span class="text-muted">Info Pembayaran</span>
                    <span class="fs-5 mt-3">Total Tagihan</span>
                    <span class="fs-5">{{number_format( intval(str_replace(',', '', $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</span>
                    <span class="fs-5">Metode Pembayaran :</span>
                    <span class="fs-5">Transfer Bank</span>
                    <span class="fs-5">Bukti Pembayaran</span>
                    <span>

                        <button type="button" class="btn btn-primary mt-10" data-bs-toggle="modal" data-bs-target="#kt_modal_1">
                            Lihat Bukti Transfer
                        </button>
                        
                        <div class="modal fade" tabindex="-1" id="kt_modal_1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title">Bukti Transfer</h3>
                        
                                        <!--begin::Close-->
                                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                            <span class="svg-icon svg-icon-1"></span>
                                        </div>
                                        <!--end::Close-->
                                    </div>
                                    <div class="modal-body">
                                        <img src="{{ asset('storage/' . $checkout->bukti_pembayaran) }}" alt="bukti-pembayaran" class="w-100">
                                    
                                    </div>
                        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </span>
                </div>
            </div>
            <!--end::Order details-->

      

            <!--begin:Order summary-->
            <div class="d-flex justify-content-between flex-column">
                <!--begin::Table-->
                <div class="table-responsive border-bottom mb-9">
                    <table class="table align-middle table-row-dashed fs-6 gy-5 mb-0">
                        <thead>
                            <tr class="border-bottom fs-6 fw-bold text-muted">
                                <th class="min-w-175px pb-2">Products</th>
                                <th class="min-w-80px text-end pb-2">Kuantitas</th>
                                <th class="min-w-100px text-end pb-2">Total</th>
                            </tr>
                        </thead>

                        <tbody class="fw-semibold text-gray-600">
                            <!--begin::Products-->
                            @foreach ($checkout->pesanans as $item)
                            <tr>
                                <!--begin::Product-->
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-5">
                                            <img src="{{ asset('storage/' . $item->produk->gambar) }}" alt="" class="w-25">
                                            <div class="fw-bold mt-3">{{ $item->produk->nm_produk}}-{{ $item->sizes->size }}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                   {{ $item->kuantitas }}
                                </td>
                                <td class="text-end">
                                    Rp.{{ number_format($item->produk->price * $item->kuantitas, 0, ',', '.') }}
                                </td>
                              
                                
                                <!--end::Total-->
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td>Ongkir </td>
                                <td class="text-end"><span>Rp.{{ number_format( $checkout->pengiriman->harga_ongkir, 0, ',', '.') }}</span></td>
                            </tr>

                            <tr>
                                <!--begin::Product-->
                                <td>
                                    <div class="d-flex align-items-center">
                                        <!--begin::Thumbnail-->
                                        
                                        <!--end::Thumbnail-->

                                        <!--begin::Title-->
                               
                                <!--end::Total-->
                            </tr>
                            <!--end::Products-->

                            <!--begin::Subtotal-->
                          
                            <!--end::Subtotal-->
                            <!--begin::Grand total-->
                            <tr>
                                <td colspan="3" class="fs-3 text-dark fw-bold text-end">
                                 Total
                                </td>
                                <td class="text-dark fs-3 fw-bolder text-end">
                                  <span>Rp.{{number_format( intval(str_replace(',', '', $checkout->total_harga)) + $checkout->pengiriman->harga_ongkir) }}</span>

                                </td>
                            </tr>
                            <!--end::Grand total-->
                        </tbody>
                    </table>
                </div>
                <!--end::Table-->
            </div>
          
         
            <!--end:Order summary-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Body-->

    <!-- begin::Footer-->
    <div class="d-flex flex-stack flex-wrap mt-lg-20 pt-13">
        <!-- begin::Actions-->
        <div class="my-1 me-5">
            <!-- begin::Pint-->
            {{-- <button type="button" class="btn btn-success my-1 me-12" onclick="window.print();">Print Invoice</button> --}}
            <!-- end::Pint-->

            <!-- begin::Download-->
            {{-- <a href="/dashbord/pesananbaru/detail-pdf/{{ $checkout->id }}" class="btn btn-light-success my-1">
                Download
            </a> --}}
             <!-- end::Download-->
        </div>
        <!-- end::Actions-->
        <a href="/dashbord/pesananbaru" class="btn btn-twitter">Kembali </a>
               <!-- begin::Action-->
      
        <!-- end::Action-->
    </div>
    <!-- end::Footer-->
</div>
<!-- end::Wrapper-->
</div>
<!-- end::Body-->
</div>
<!-- end::Invoice 1-->        </div>
<!--end::Content container-->
</div>
@endsection