@extends('dashbord.layouts.main')

@section('container')

<div id="kt_app_content" class="app-content  flex-column-fluid ">
    <!--begin::Content container-->
<div id="kt_app_content_container" class="app-container  container-xxl ">
    <!--begin::Card-->
<div class="card">
<!--begin::Card header-->
<div class="card-header border-0 pt-6">
<!--begin::Card title-->
<div class="card-title">
    <!--begin::Search-->
    <div class="d-flex align-items-center position-relative my-1">
        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
<span class="svg-icon svg-icon-1 position-absolute ms-6"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
</svg>
</span>
<!--end::Svg Icon-->                <input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Customers">
    </div>
    <!--end::Search-->
</div>
<!--begin::Card title-->

<!--begin::Card toolbar-->
<div class="card-toolbar">
    <!--begin::Toolbar-->
<div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">
<!--begin::Filter-->
<!--end::Menu 1-->    <!--end::Filter-->

<!--begin::Export-->

<!--begin::Add customer-->

<!--end::Add customer-->
</div>
<!--end::Toolbar-->

<!--begin::Group actions-->
<div class="d-flex justify-content-end align-items-center d-none" data-kt-customer-table-toolbar="selected">
<div class="fw-bold me-5">
<span class="me-2" data-kt-customer-table-select="selected_count"></span> Selected
</div>

<button type="button" class="btn btn-danger" data-kt-customer-table-select="delete_selected">
Delete Selected
</button>
</div>
<!--end::Group actions-->        </div>
<!--end::Card toolbar-->
</div>
<!--end::Card header-->

<!--begin::Card body-->
<div class="card-body pt-0">

<!--begin::Table-->
<div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="table-responsive"><table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_customers_table">
<!--begin::Table head-->
<thead>
<!--begin::Table row-->
<tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0"><th class="w-10px pe-2 sorting_disabled" rowspan="1" colspan="1" aria-label="
        
            
        
    " style="width: 29.8906px;">
        
    </th><th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Customer Name: activate to sort column ascending" style="width: 135.156px;">Nama</th><th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 167.703px;">Alamat</th><th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Company: activate to sort column ascending" style="width: 148.75px;">kode pos</th><th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Payment Method: activate to sort column ascending" style="width: 137.359px;">Nama Penerima</th><th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Created Date: activate to sort column ascending" style="width: 176.141px;">total harga</th><th class="text-end min-w-70px sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 105.5px;">Actions</th></tr>
<!--end::Table row-->
</thead>
<!--end::Table head-->

<!--begin::Table body-->
<tbody class="fw-semibold text-gray-600">
    @foreach ($checkout as $item)
    <tr class="odd">
        <!--begin::Checkbox-->
       <td></td>
        <td class="text-gray-800 text-hover-primary mb-1">{{ $item->pengiriman->nama }}   </td>
        <td class="text-gray-600 text-hover-primary mb-1">{{ $item->pengiriman->alamat }}</td>
        <td data-filter="mastercard">    {{ $item->pengiriman->kode_pos }}    </td>
        <td data-order="2020-12-14T20:43:00+07:00"> {{ $item->pengiriman->nm_penerima }}</td>
        <td>{{number_format( intval(str_replace(',', '',  $item->total_harga)) + $item->pengiriman->harga_ongkir) }}</td>

     
               
        <!--begin::Action--->
        
        <td class="text-end">
            <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                Actions
                <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
<span class="svg-icon svg-icon-5 m-0"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="currentColor"></path>
</svg>
</span>

<!--end::Svg Icon-->         
            </a>
            <!--begin::Menu-->
 
<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true" style="">
<!--begin::Menu item-->
<div class="menu-item px-3">
<div class="menu-link px-3">
    <a href="pesananbaru/{{ $item->id }}/detail">Detail</a> 
</div>
</div>

<!--end::Menu item-->

<!--begin::Menu item-->
<div class="menu-item px-3">
<div class="menu-link px-3" data-kt-customer-table-filter="delete_row">
    <form
                   action="{{ url('dashbord/pesananbaru/'.$item->id.'/updatestatus') }}"
                    method="POST" >
                      @csrf
                 <input type="hidden" name="status"
                          value="Sedang DiProses">
                      <button type="submit"
               class="btn-sm btn btn-warning menu-link px-1 text-white justify-content-center">
             <i class="fas fa-check"></i>konfirmasi</button>
 </form>
</div>
</div>
<!--end::Menu item-->
</div>
<!--end::Menu-->
        </td>
        <!--end::Action--->
          @endforeach
   
    </tr><tr class="even">

      
    
        <!--end::Form-->
    </div>
    <!--end::Modal body-->
</div>
<!--end::Modal content-->
</div>
<!--end::Modal dialog-->
</div>
<!--end::Modal - New Card--><!--end::Modals-->        </div>
<!--end::Content container-->
</div>

@endsection
