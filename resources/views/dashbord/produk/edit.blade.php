@extends('dashbord.layouts.main')

@section('container')
<div class="container">
    <div class="card card-custom card-create">
        <div class="card-body">
            <form action="{{ url('dashbord/produk/' . $produk->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PATCH">

                 <div class="row">
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">Nama Produk :</label> 
                            <input type="text" placeholder="Nama Produk" name="nm_produk" autocomplete="off" required 
                              @error('nm_produk')is-invalid @enderror class="form-control" value="{{ old('nm_produk', $produk->nm_produk) }}">
                            @error('nm_produk')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">kategori :</label> 
                            <select name="kategori_id" id="kategori_id" class="form-select" data-control="select2" data-placeholder="Pilih Kategori">
                              @foreach ($kategori as $item)
                                <option value="{{ $item->id }}" {{ ($item->id == $produk->kategori_id) ? 'selected' : '' }}>{{ $item->nm_kategori }}</option>
                              @endforeach
                            </select>
                        </div>
                    </div> 
                 
                    <div class="col-md-4">
                      <div class="mb-4">
                          <label class="required form-label">Berat :</label> 
                          <input type="text" placeholder="Berat" name="berat" required="required" @error('berat')is-invalid @enderror class="form-control" value="{{ old('berat', $produk->berat) }}">
                          @error('berat')
                          <div class="invalid-feedback d-block">
                            {{ $message }}
                          </div>
                        @enderror
                      </div>
                  </div>
                    <div class="col-md-4">
                      <div class="mb-4">
                          <label class="required form-label">Price :</label> 
                          <input type="number" placeholder="Price" autocomplete="off" name="price" required="required" @error('price')is-invalid @enderror class="form-control" value="{{ old('price', $produk->price) }}">
                          @error('price')
                          <div class="invalid-feedback d-block">
                            {{ $message }}
                          </div>
                        @enderror
                      </div>
                  </div> 
                  <div class="col-md-4 mb-3">
                    <label class="required form-label">Image</label> 
                    <input type="file" accept="jpg, png, jpeg" name="gambar" class="form-control" value="{{ old('gambar', $produk->gambar) }}">
                  </div>
                      <div class="col-md-12">
                        <div class="mb-4">
                            <label class="required form-label">Deskripsi :</label> 
                            <textarea type="text"  placeholder="Deskripsi" autocapitalize="off" name="deskripsi" required="required" @error('deskripsi')is-invalid @enderror class="form-control">{{ old('deskripsi', $produk->deskripsi) }}</textarea>
                            @error('deskripsi')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                      </div> 
             
                           
                      <div id="size">
                        <!--begin::Form group-->
                        <div class="form-group">
                          <div data-repeater-list="size">
                            @foreach ($produk->size as $size)
                              <div data-repeater-item>
                                  <div class="form-group row">
                                      <div class="col-8">
                                          <div class="row">
                                             <input type="hidden" name="id" value="{{ $size->id }}">
                                              <div class="col-md-3 mb-3">
                                                  <label class="form-label">Size :</label>
                                                  <input type="number" name="size"
                                                      class="form-control mb-2 mb-md-0" placeholder="Size" value="{{ old('size', $size->size) }}" />
                                              </div>
                                              <div class="col-md-3 mb-3">
                                                  <label class="form-label">Stock :</label>
                                                  <input type="text" name="stock"
                                                      class="form-control mb-2 mb-md-0" placeholder="stock" value="{{ old('stock', $size->stock) }}" />
                                              </div>
                                              <div class="col-md-3">
                                                  <a href="javascript:;" data-repeater-delete
                                                      class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                      <i class="la la-trash-o"></i>Delete
                                                  </a>
                                              </div>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                              @endforeach
                          </div>
                        </div>
                        <!--end::Form group-->

                        <!--begin::Form group-->
                        <div class="form-group mt-5">
                            <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                                <i class="la la-plus"></i>Add
                            </a>
                        </div>
                        <!--end::Form group-->
                    </div>
                       <div class="kt-form__actions mt-5">
                         <button type="submit" class="btn btn-primary btn-sm mb-2 me-2">
                           <i class="las la-save fs-2 me-2"></i> SAVE</button> 
                         <a href="/dashbord/produk/"  type="button" class="btn btn-danger btn-sm mb-2 me-2">
                           <i class="las la-ban fs-2 me-2"> </i> CANCEL
                         </a>
                   </div>
             </form>
       </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
<script>
$('#size').repeater({
  initEmpty: false,

  show: function() {
      $(this).slideDown();
  },

  hide: function(deleteElement) {
      $(this).slideUp(deleteElement);
  }
});
</script>
@endsection
