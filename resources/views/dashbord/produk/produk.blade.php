@extends('dashbord.layouts.main')

@section('container')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-xxl">
        <!--begin::Category-->
        <div class="card card-flush">
            <!--begin::Card header-->
            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                <!--begin::Card title-->
                <div class="card-title">
                    <!--begin::Search-->
                    <form action="{{ url('dashbord/produk') }}" method="GET">
                        <div class="d-flex align-items-center position-relative my-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                            <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                                </svg>
                            </span>     
                            <!--end::Svg Icon-->
                            <input type="text" name="keyword" data-kt-ecommerce-category-filter="search"
                            class="form-control form-control-solid w-250px ps-14" placeholder="Cari Produk"
                            value="{{ $keyword }}" autocomplete="off">
                        </div>
                    </form>
                
                    <!--end::Search-->
                </div>
                <!--end::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Add customer-->
                    <a href="/dashbord/produk/create" class="btn btn-success">Tambah Produk   <i class="bi bi-plus-lg"></i></a>
                    <!--end::Add customer-->
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body pt-0">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table table-hover table-rounded table-striped border gy-7 gs-7">
                     <thead>
                      <tr class="fw-semibold fs-6 text-gray-800 border-bottom-2 border-gray-200">
                       <th>Nama Produk</th>
                       <th>Kategori</th>
                       <th>Berat</th>
                       <th>Harga</th>
                       <th>Size dan Stok</th>
                       <th>Deskripsi</th>
                       <th>Gambar</th>   
                      </tr>
                     </thead>
                     <tbody>
                        @foreach ($produk as $produk)
                            <tr>
                                <td>{{ $produk->nm_produk }}</td>
                                <td>{{ $produk->kategori->nm_kategori }}</td>
                                <td>{{ $produk->berat }}</td>
                                <td>{{format_uang($produk->price)}}</td>
                                <td>{{ $produk->size }}-{{ $produk->stock }}</td>
                                <td>{{ $produk->deskripsi }}</td>

                                
                                <td>
                                    <img src="{{ asset('storage/' .  $produk->gambar ) }}" alt="gambar" class="img-fluid" width="100">
                                </td>
                                <td class="text-end">
                                    <form action="{{ route('produk.destroy', $produk->id) }}" method="post" class="d-inline">
                                        @method('delete')
                                        @csrf
                                    <button type="submit" class="btn btn-danger border-0 btn-sm mb-2 me-2 mt-10" onclick="return confirm('apakah anda yakin menghapus data ini ?')">Hapus</button>

                                
                                    <a href="{{ url('dashbord/produk/' .$produk->id. '/edit') }}" class="btn btn-primary btn-sm mb-2 me-2 mt-10">Edit</a>
                                </td>
                                </td>
                             
                            </tr>
                        @endforeach
                    </form>

                    </tbody>
                    </table>
                   </div>
                </div>
               
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Category-->
    </div>
    <!--end::Content container-->
</div>

@endsection