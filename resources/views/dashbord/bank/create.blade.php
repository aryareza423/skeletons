@extends('dashbord.layouts.main')

@section('container')
<div class="container">
    <div class="card card-custom card-create">
        <div class="card-body">
            <form action="{{ route('bank.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                 <div class="row">
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">Nama Bank :</label> 
                            <input type="text" placeholder="Nama Bank" name="nm_bank" autocomplete="off" required="required" @error('nm_bank')is-invalid @enderror class="form-control">
                            @error('nm_bank')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div> 
                     
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">No Rekening :</label> 
                            <input type="text" placeholder="No Rekening" autocomplete="off" name="no_rekening" required="required" @error('no_rekening')is-invalid @enderror class="form-control">
                            @error('no_rekening')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div> 
                    <div class="col-md-6">
                      <div class="mb-4">
                          <label class="required form-label">Atas Nama :</label> 
                          <input type="text" placeholder="Atas Nama" name="atas_nama" required="required" @error('atas_nama')is-invalid @enderror class="form-control">
                          @error('atas_nama')
                          <div class="invalid-feedback d-block">
                            {{ $message }}
                          </div>
                        @enderror
                      </div>
                  </div>
                      <div class="col-md-6 mb-3">
                        <label class="required form-label">Image</label> 
                        <input type="file" accept="jpg, png, jpeg" name="gambar" class="form-control">
                      </div>
                       <div class="kt-form__actions mt-5">
                         <button type="submit" class="btn btn-primary btn-sm mb-2 me-2">
                           <i class="las la-save fs-2 me-2"></i> SAVE</button> 
                         <a href="/dashbord/bank/"  type="button" class="btn btn-danger btn-sm mb-2 me-2">
                           <i class="las la-ban fs-2 me-2"> </i> CANCEL
                         </a>
                   </div>
             </form>
       </div>
    </div>
</div>

@endsection