@extends('dashbord.layouts.main')

@section('container')
<div class="container">
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
                                
<!--begin::Toolbar-->
<div id="kt_app_toolbar" class="app-toolbar  d-flex pb-3 pb-lg-5 ">

<!--begin::Toolbar container-->
<div class="d-flex flex-stack flex-row-fluid">     
<!--begin::Toolbar container-->
<div class="d-flex flex-column flex-row-fluid">     
<!--begin::Toolbar wrapper-->                   

<!--begin::Page title-->
<div class="page-title d-flex align-items-center me-3">
<!--begin::Title-->


        <!--begin::Description-->

<!--end::Title-->    
</div>
<!--end::Page title-->  
    


</div>
<!--end::Toolbar container-->

<!--begin::Actions-->
<div class="d-flex align-self-center flex-center flex-shrink-0">



</div>
<!--end::Actions-->  
</div>
<!--end::Toolbar container-->    </div>
<!--end::Toolbar-->                                        
        
<!--begin::Content-->
<div id="kt_app_content" class="app-content  flex-column-fluid ">
<!--begin::Row-->
<div class="row g-5 g-xl-10 mb-5 mb-xl-0">
<!--begin::Col-->
<div class="col-md-4 mb-xl-10">
<!--begin::Card widget 28-->
<div class="card card-flush ">
<!--begin::Header-->
<div class="card-header pt-7"> 
<!--begin::Card title-->
<div class="card-title flex-stack flex-row-fluid">                    
<!--begin::Symbol-->
<div class="symbol symbol-45px me-5">
    <span class="symbol-label bg-light-info">
        <!--begin::Svg Icon | path: icons/duotune/social/soc005.svg-->
<span class="svg-icon svg-icon-2x svg-icon-gray-800"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M512 80c0 18-14.3 34.6-38.4 48c-29.1 16.1-72.5 27.5-122.3 30.9c-3.7-1.8-7.4-3.5-11.3-5C300.6 137.4 248.2 128 192 128c-8.3 0-16.4 .2-24.5 .6l-1.1-.6C142.3 114.6 128 98 128 80c0-44.2 86-80 192-80S512 35.8 512 80zM160.7 161.1c10.2-.7 20.7-1.1 31.3-1.1c62.2 0 117.4 12.3 152.5 31.4C369.3 204.9 384 221.7 384 240c0 4-.7 7.9-2.1 11.7c-4.6 13.2-17 25.3-35 35.5c0 0 0 0 0 0c-.1 .1-.3 .1-.4 .2l0 0 0 0c-.3 .2-.6 .3-.9 .5c-35 19.4-90.8 32-153.6 32c-59.6 0-112.9-11.3-148.2-29.1c-1.9-.9-3.7-1.9-5.5-2.9C14.3 274.6 0 258 0 240c0-34.8 53.4-64.5 128-75.4c10.5-1.5 21.4-2.7 32.7-3.5zM416 240c0-21.9-10.6-39.9-24.1-53.4c28.3-4.4 54.2-11.4 76.2-20.5c16.3-6.8 31.5-15.2 43.9-25.5V176c0 19.3-16.5 37.1-43.8 50.9c-14.6 7.4-32.4 13.7-52.4 18.5c.1-1.8 .2-3.5 .2-5.3zm-32 96c0 18-14.3 34.6-38.4 48c-1.8 1-3.6 1.9-5.5 2.9C304.9 404.7 251.6 416 192 416c-62.8 0-118.6-12.6-153.6-32C14.3 370.6 0 354 0 336V300.6c12.5 10.3 27.6 18.7 43.9 25.5C83.4 342.6 135.8 352 192 352s108.6-9.4 148.1-25.9c7.8-3.2 15.3-6.9 22.4-10.9c6.1-3.4 11.8-7.2 17.2-11.2c1.5-1.1 2.9-2.3 4.3-3.4V304v5.7V336zm32 0V304 278.1c19-4.2 36.5-9.5 52.1-16c16.3-6.8 31.5-15.2 43.9-25.5V272c0 10.5-5 21-14.9 30.9c-16.3 16.3-45 29.7-81.3 38.4c.1-1.7 .2-3.5 .2-5.3zM192 448c56.2 0 108.6-9.4 148.1-25.9c16.3-6.8 31.5-15.2 43.9-25.5V432c0 44.2-86 80-192 80S0 476.2 0 432V396.6c12.5 10.3 27.6 18.7 43.9 25.5C83.4 438.6 135.8 448 192 448z"/></svg>
</span>
<!--end::Svg Icon-->                </span>                
</div>
<!--end::Symbol-->             

<!--begin::Wrapper-->

<!--end::Wrapper-->      
</div>
<!--end::Header-->            
</div>
<!--end::Card title-->

<!--begin::Card body-->
<div class="card-body d-flex align-items-end">             
<!--begin::Wrapper-->
<div class="d-flex flex-column">             
<span class="fw-bolder fs-2x text-dark">Rp.{{ format_uang($totalPendapatan, 0, ',', '.') }}</span>
<span class="fw-bold fs-7 text-gray-500">Pendapatan Penjualan</span>       
</div>
<!--end::Wrapper-->      
</div>
<!--end::Card body-->
</div>
<!--end::Card widget 28-->    </div>
<!--end::Col-->

<!--begin::Col-->
<div class="col-md-4 mb-xl-10">
<!--begin::Card widget 28-->
<div class="card card-flush ">
<!--begin::Header-->
<div class="card-header pt-7"> 
<!--begin::Card title-->
<div class="card-title flex-stack flex-row-fluid">                    
<!--begin::Symbol-->
<div class="symbol symbol-45px me-5">
    <span class="symbol-label bg-light-info">
        <!--begin::Svg Icon | path: icons/duotune/social/soc003.svg-->
<span class="svg-icon svg-icon-2x svg-icon-gray-800"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48H48zM0 176V384c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V176L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z"/></svg>
</span>
<!--end::Svg Icon-->                </span>                
</div>
<!--end::Symbol-->             

<!--begin::Wrapper-->

<!--end::Wrapper-->      
</div>
<!--end::Header-->            
</div>
<!--end::Card title-->

<!--begin::Card body-->
<div class="card-body d-flex align-items-end">             
<!--begin::Wrapper-->
<div class="d-flex flex-column">             
<span class="fw-bolder fs-2x text-dark">{{ $checkout->where('status', 'Pesanan Baru')->count() }}</span>
<span class="fw-bold fs-7 text-gray-500">Pesanan</span>       
</div>
<!--end::Wrapper-->      
</div>
<!--end::Card body-->
</div>
<!--end::Card widget 28-->    </div>
<!--end::Col-->   

<!--begin::Col-->
<div class="col-md-4 mb-xl-10">
<!--begin::Card widget 28-->
<div class="card card-flush ">
<!--begin::Header-->
<div class="card-header pt-7"> 
<!--begin::Card title-->
<div class="card-title flex-stack flex-row-fluid">                    
<!--begin::Symbol-->
<div class="symbol symbol-45px me-5">
    <span class="symbol-label bg-light-info">
        <!--begin::Svg Icon | path: icons/duotune/social/soc001.svg-->
<span class="svg-icon svg-icon-2x svg-icon-gray-800"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M112 0C85.5 0 64 21.5 64 48V96H16c-8.8 0-16 7.2-16 16s7.2 16 16 16H64 272c8.8 0 16 7.2 16 16s-7.2 16-16 16H64 48c-8.8 0-16 7.2-16 16s7.2 16 16 16H64 240c8.8 0 16 7.2 16 16s-7.2 16-16 16H64 16c-8.8 0-16 7.2-16 16s7.2 16 16 16H64 208c8.8 0 16 7.2 16 16s-7.2 16-16 16H64V416c0 53 43 96 96 96s96-43 96-96H384c0 53 43 96 96 96s96-43 96-96h32c17.7 0 32-14.3 32-32s-14.3-32-32-32V288 256 237.3c0-17-6.7-33.3-18.7-45.3L512 114.7c-12-12-28.3-18.7-45.3-18.7H416V48c0-26.5-21.5-48-48-48H112zM544 237.3V256H416V160h50.7L544 237.3zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48s48 21.5 48 48s-21.5 48-48 48zm368-48c0 26.5-21.5 48-48 48s-48-21.5-48-48s21.5-48 48-48s48 21.5 48 48z"/></svg>
</span>
<!--end::Svg Icon-->                </span>                
</div>
<!--end::Symbol-->             

<!--begin::Wrapper-->

<!--end::Wrapper-->      
</div>
<!--end::Header-->            
</div>
<!--end::Card title-->

<!--begin::Card body-->
<div class="card-body d-flex align-items-end">             
<!--begin::Wrapper-->
<div class="d-flex flex-column">             
<span class="fw-bolder fs-2x text-dark">{{ $checkout->where('status', 'Dalam Pengiriman')->count() }}</span>
<span class="fw-bold fs-7 text-gray-500">Dikirim</span>       
</div>
<!--end::Wrapper-->      
</div>
<!--end::Card body-->
</div>
<!--end::Card widget 28-->    </div>
<!--end::Col-->   
</div>
<!--end::Row--> 



      


<!--begin::Card widget 7-->
<div class="card card-flush h-md-50 mb-lg-10">
<!--begin::Header-->
<div class="card-header pt-5">
<!--begin::Title-->
<div class="card-title d-flex flex-column">                
<!--begin::Amount-->
<span class="fs-2hx fw-bold text-dark me-2 lh-1 ls-n2">{{ $user->count() }}</span>
<!--end::Amount-->           

<!--begin::Subtitle-->
<span class="text-gray-400 pt-1 fw-semibold fs-6">Customer Yang Terdaftar</span>
<!--end::Subtitle--> 
</div>
<!--end::Title-->           
</div>
<!--end::Header-->

<!--begin::Card body-->
<div class="card-body d-flex flex-column justify-content-end pe-0">
<!--begin::Title-->
<span class="fs-6 fw-bolder text-gray-800 d-block mb-2">User </span>
<!--end::Title-->

<!--begin::Users group-->
<div class="symbol-group symbol-hover flex-nowrap">
    @foreach ($user as $item)
    <div class="symbol symbol-35px symbol-circle" data-bs-toggle="tooltip" data-bs-original-title="{{ $item->nama }}" data-kt-initialized="1">
        <span class="symbol-label bg-warning text-inverse-warning fw-bold">{{ $item->nama }}</span>
</div>
    @endforeach
    <span class="symbol-label bg-light text-gray-400 fs-8 fw-bold">+{{ $user->count() }}</span>
</a>
</div>
<!--end::Users group-->
</div>
<!--end::Card body-->
</div>
<!--end::Card widget 7-->    </div>
<!--end::Col-->
</div>
<!--end::Row-->      </div>
<!--end::Content-->			
	
    </div>
    
    <!--end::Content wrapper-->

                        
<!--begin::Footer-->
<div id="kt_app_footer" class="app-footer  d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4 ">
<!--begin::Copyright-->
<div class="text-dark order-2 order-md-1">
</div>
<!--end::Copyright-->

<!--begin::Menu-->
<!--end::Menu-->    </div>
<!--end::Footer-->                            </div>

</div>
@endsection