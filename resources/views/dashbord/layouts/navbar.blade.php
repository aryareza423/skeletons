<header class="header trans_300">
    
  <!-- Top Navigation -->

  <div class="top_nav">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
        
          <div class="top_nav_left" style="font-size: initial; font-family:none;"> SELAMAT BELANJA DI TOKO KAMI  </div>
         
        </div>
        <div class="col-md-6 text-right">
          <div class="top_nav_right">
            <ul class="top_nav_menu">

              <!-- Currency / Language / My Account -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- navbar -->
  <div class="main_nav_container">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-right">
          <div class="logo_container">
  
         <img src="{{ asset('assets/media/logoproduk/logoskeletons.png') }}" alt="" width="90" height="90" class="me-2" />
          <span style="font-size:1.5rem; font-family: fantasy;">SKELETONS</span>
          </div>
          <nav class="navbar">
            <ul class="navbar_menu">
              @guest                 
              <li><a href="/register" style="font-size: 12px">Daftar</a></li>
              @endguest
              <li><a href="/">Home</a></li>
              {{-- <li><a href="/pesanan">Pesanan</a></li> --}}
              <li><a href="/pesanan" class="text-dark">Pesanan</a></li>
              @if (auth()->user() && !auth()->user()->is_admin)
              <li><a href="/profile">Profile</a></li>
              @endif
            </ul>
            <ul class="navbar_user">
              @auth
              <li><a href="/logout"><i class="bi bi-box-arrow-in-right"></i></a></li>   
              @endauth        
              <li>
                <form action="{{ url('/') }}" method="GET">
                  <input type="text" class="form-control search-form d-none" name="keyword" placeholder="Cari Produk" value="{{ $keyword }}" autocomplete="off">
                </form>
              </li>
              <li><a href="#" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></a></li>
            @can('admin')
            <li><a href="/dashbord/index"><i class="fa fa-user" aria-hidden="true"></i></a></li>
            @endcan
            @auth  
            <li class="checkout">
              <a href="/keranjang">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <span id="checkout_items" class="checkout_items">
                  {{ \App\Models\Keranjang::where('user_id', auth()->user()->id)->count() }}
                </span>
              </a>
            </li>
            @endauth
            </ul>
        
            <div class="hamburger_container">
              <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>

  </header>
 
<div class="hamburger_menu">
  <div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
  <div class="hamburger_menu_content text-right">
    <ul class="menu_top_nav">
      <li class="menu_item has-children">
      </li>
      @guest
      <li class="menu_item"><a href="/register">Daftar</a></li>
      @endguest
      <li class="menu_item"><a href="/">Home</a></li>
              <li><a href="/logout"><i class="bi bi-box-arrow-in-right"></i></a></li>           
              <li class="menu_item"><a href="#"></a></li>
      <li>
        <form action="/katalog">
          <input type="text" class="form-control search-form d-none" name="search">
        </form>
      </li>
      <li><a href="#" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></a></li>
    </ul>
  </div>
</div>

<script>
  document.querySelector('.search-icon').addEventListener('click', function () {
    document.querySelector('.search-form').classList.toggle('d-none');
  });
</script>