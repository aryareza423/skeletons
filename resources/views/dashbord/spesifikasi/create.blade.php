@extends('dashbord.layouts.main')

@section('container')
<div class="container">
    <div class="card card-custom card-create">
        <div class="card-body">
            <form action="{{ url('dashbord/spesifikasiproduk') }}" method="POST" enctype="multipart/form-data">
                @csrf
                 <div class="row">
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">Merek</label> 
                            <input type="text" placeholder="Merek" name="merek" autocomplete="off" required="required" @error('merek')is-invalid @enderror class="form-control">
                            @error('merek')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div> 
                      <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">Produk :</label> 
                            <select name="produk_id" id="produk_id" class="form-select" data-control="select2" data-placeholder="Pilih Produk">
                            <option value=""></option>
                              @foreach ($dataProduk as $item)
                                <option value="{{ $item->id }}">{{ $item->nm_produk }}</option>
                              @endforeach
                            </select>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="mb-4">
                            <label class="required form-label">Model Sepatu :</label> 
                            <input type="text" placeholder="Model Sepatu" autocomplete="off" name="model_sepatu" required="required" @error('model_sepatu')is-invalid @enderror class="form-control">
                            @error('model_sepatu')
                            <div class="invalid-feedback d-block">
                              {{ $message }}
                            </div>
                          @enderror
                        </div>
                    </div> 
                      <div class="col-md-6">
                          <div class="mb-4">
                              <label class="required form-label">Negara Asal :</label> 
                              <input type="text" placeholder="Negara Asal" name="negara_asal" required="required" @error('negara_asal')is-invalid @enderror class="form-control">
                              @error('negara_asal')
                              <div class="invalid-feedback d-block">
                                {{ $message }}
                              </div>
                            @enderror
                          </div>
                      </div> 
                 
                    {{-- <div class="col-md-4 mb-3">
                      <label class="required form-label">Image</label> 
                      <input type="file" accept="jpg, png, jpeg" name="gambar" class="form-control">
                    </div> --}}
                    <div id="gambar_spesifikasi">
                      <!--begin::Form group-->
                      <div class="form-group">
                          <div data-repeater-list="gambar_spesifikasi">
                              <div data-repeater-item>
                                  <div class="form-group row">
                                      <div class="col-8">
                                          <div class="row">
                                              <div class="col-md-3 mb-3">
                                                  <div class="image-input image-input-empty"
                                                      data-kt-image-input="true"
                                                      style="background-image: url(/assets/media/svg/avatars/blank.svg)">
                                                      <!--begin::Image preview wrapper-->
                                                      <div class="image-input-wrapper w-125px h-125px"></div>
                                                      <!--end::Image preview wrapper-->

                                                      <!--begin::Edit button-->
                                                      <label
                                                          class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                          data-kt-image-input-action="change"
                                                          data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                          title="Change avatar">
                                                          <i class="bi bi-pencil-fill fs-7"></i>

                                                          <!--begin::Inputs-->
                                                          <input type="file" name="gambar"
                                                              accept=".png, .jpg, .jpeg" />
                                                          {{-- <input type="hidden" name="gambar" /> --}}
                                                          <!--end::Inputs-->
                                                      </label>
                                                      <!--end::Edit button-->
                                                      <!--begin::Cancel button-->
                                                      <span
                                                          class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                          data-kt-image-input-action="cancel"
                                                          data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                          title="Cancel avatar">
                                                          <i class="bi bi-x fs-2"></i>
                                                      </span>
                                                      <!--end::Cancel button-->
                                                      <!--begin::Remove button-->
                                                      <span
                                                          class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                                          data-kt-image-input-action="remove"
                                                          data-bs-toggle="tooltip" data-bs-dismiss="click"
                                                          title="Remove avatar">
                                                          <i class="bi bi-x fs-2"></i>
                                                      </span>
                                                      <!--end::Remove button-->
                                                  </div>
                                              </div>
                                           <div class="col-md-3">
                                                  <a href="javascript:;" data-repeater-delete
                                                      class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                      <i class="la la-trash-o"></i>Delete
                                                  </a>
                                              </div>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>
                      <!--end::Form group-->

                      <!--begin::Form group-->
                      <div class="form-group mt-5">
                          <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                              <i class="la la-plus"></i>Add
                          </a>
                      </div>
                      <!--end::Form group-->
                  </div>
                       <div class="kt-form__actions mt-5">
                         <button type="submit" class="btn btn-primary btn-sm mb-2 me-2">
                           <i class="las la-save fs-2 me-2"></i> SAVE</button> 
                         <a href="/dashbord/spesifikasiproduk/"  type="button" class="btn btn-danger btn-sm mb-2 me-2">
                           <i class="las la-ban fs-2 me-2"> </i> CANCEL
                         </a>
                   </div>
             </form>
       </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
<script>
$('#gambar_spesifikasi').repeater({
            initEmpty: false,

            show: function() {
                $(this).slideDown();
                setTimeout(() => {
                  KTImageInput.createInstances();
                }, 100);
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
</script>
@endsection
