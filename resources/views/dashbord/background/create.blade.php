@extends('dashbord.layouts.main')

@section('container')
<div class="container">
    <div class="card card-custom card-create">
        <div class="card-body">
            <form action="{{ url('dashbord/background') }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4 mb-3">
                          <label class="required form-label">Image</label> 
                          <input type="file" accept="jpg, png, jpeg" name="gambar" class="form-control">
                        </div>
                        <div class="kt-form__actions mt-5">
                          <button type="submit" class="btn btn-primary btn-sm mb-2 me-2">
                            <i class="las la-save fs-2 me-2"></i> SAVE</button> 
                          <a href="/dashbord/background/"  type="button" class="btn btn-danger btn-sm mb-2 me-2">
                            <i class="las la-ban fs-2 me-2"> </i> CANCEL
                          </a>
                    </div>
                  </div>
            </form>
@endsection