@extends('dashbord.layouts.main')

@section('container')
    <div id="kt_app_content" class="app-content flex-column-fluid">
        <!--begin::Content container-->
        <div id="kt_app_content_container" class="app-container container-xxl">
            <!--begin::Category-->
            <div class="card card-flush">
                <!--begin::Card header-->
                <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                    <!--begin::Card title-->
                    <div class="card-title">
                        <!--begin::Search-->
                        <div class="d-flex align-items-center position-relative my-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                            {{-- <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                        rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
                                    <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"></path>
                                </svg> --}}
                            </span>
                            <!--end::Svg Icon-->
                            {{-- <input type="text" data-kt-ecommerce-category-filter="search"
                                class="form-control form-control-solid w-250px ps-14" placeholder="Cari Category"> --}}
                        </div>
                        <!--end::Search-->
                    </div>
                    <!--end::Card title-->
                    <!--begin::Card toolbar-->
                    <div class="card-toolbar">
                        <!--begin::Add customer-->
                        <a href="/dashbord/background/create" class="btn btn-success">Tambah Background  <i class="bi bi-plus-lg"></i></a>
                        <!--end::Add customer-->
                    </div>
                    <!--end::Card toolbar-->
                </div>
                <!--end::Card header-->
                <!--begin::Card body-->
                <div class="card-body pt-0">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table table-hover table-rounded table-striped border gy-7 gs-7">
                            <thead>
                                <tr class="fw-semibold fs-6 text-gray-800 border-bottom-2 border-gray-200">
                                    <th class="text-center">Gambar</th>
                                  
                               
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($background as $background)
                                    <tr>
                                        <td class="text-center">
                                            <img src="{{ asset('storage/' .  $background->gambar ) }}" alt="gambar" class="img-fluid" width="100">
                                        </td>
                                        <td>
                                        <td class="text-end">
                                            <form action="{{ route('background.destroy', $background->id) }}" method="post" class="d-inline">
                                                @method('delete')
                                                @csrf
                                            <button type="submit" class="btn btn-danger border-0 btn-sm mb-2 me-2 mt-10" onclick="return confirm('apakah anda yakin menghapus data ini ?')">Hapus</button>
                                            </form>

                                        <a href="{{ url('dashbord/background/' .$background->id. '/edit') }}" class="btn btn-primary btn-sm mb-2 me-2 mt-10">Edit</a>
                                        </td>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- <div class="row">
                    <div
                        class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start">
                        <div class="dataTables_length" id="kt_ecommerce_category_table_length"><label><select
                                    name="kt_ecommerce_category_table_length" aria-controls="kt_ecommerce_category_table"
                                    class="form-select form-select-sm form-select-solid">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select></label></div>
                    </div>
                    <div class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">
                        <div class="dataTables_paginate paging_simple_numbers" id="kt_ecommerce_category_table_paginate">
                            <ul class="pagination">
                                <li class="paginate_button page-item previous disabled"
                                    id="kt_ecommerce_category_table_previous"><a href="#"
                                        aria-controls="kt_ecommerce_category_table" data-dt-idx="0" tabindex="0"
                                        class="page-link"><i class="previous"></i></a></li>
                                <li class="paginate_button page-item active"><a href="#"
                                        aria-controls="kt_ecommerce_category_table" data-dt-idx="1" tabindex="0"
                                        class="page-link">1</a></li>
                                <li class="paginate_button page-item "><a href="#"
                                        aria-controls="kt_ecommerce_category_table" data-dt-idx="2" tabindex="0"
                                        class="page-link">2</a></li>
                                <li class="paginate_button page-item next" id="kt_ecommerce_category_table_next"><a
                                        href="#" aria-controls="kt_ecommerce_category_table" data-dt-idx="3"
                                        tabindex="0" class="page-link"><i class="next"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!--end::Table-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Category-->
    </div>
    <!--end::Content container-->
    </div>
@endsection
