
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.bundle.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/global/plugins.bundle.css') }}">
    <title>Toko Online</title>
  </head>
  <body>
    <div class="container d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
    <form class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework"  action="/login" method="post" class="form-container">
        @csrf
      <!--begin::Form-->
      @if (session()->has ('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
      </div>
      @endif
      @if (session()->has ('loginEror'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('loginEror') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
      </div>
      @endif
      <div class="d-flex flex-center flex-column flex-lg-row-fluid">
        <div class="w-lg-500px p-10">
            <div class="text-center mb-11">  
              <h1 class="text-dark fw-bolder mb-3">Login</h1>
              <div class="text-gray-500 fw-semibold fs-6">Silakan Login</div>
            </div>
            <div class="separator separator-content my-14">
              <span class="w-125px text-gray-500 fw-semibold fs-7">Masukkan Email Anda!</span>
            </div>
            <div class="fv-row mb-8 fv-plugins-icon-container">
              <!--begin::Email-->
                <div class="mb-5">
              <input type="email" class="form-control bg-transparent"  @error('email')is-invalid @enderror" name="email" id="exampleInputEmail1" autocomplete="off" aria-describedby="emailHelp" placeholder="email"  required value="{{ old('email') }}" />
              @error('email')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>
            <div class="fv-plugins-message-container invalid-feedback"></div></div>
            <div class="fv-row mb-8 fv-plugins-icon-container" data-kt-password-meter="true">
              <div class="mb-1">
                <div class="position-relative mb-3">
                  <input type="password" class="form-control" name="password" id="exampleInputPassword1" autocomplete="off" placeholder="password" />
                  <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                  </span>
                </div>
                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                </div>
                <!--end::Meter-->
              </div>
              <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
              <!--end::Hint-->
            <div class="fv-plugins-message-container invalid-feedback"></div></div>

            <div class="fv-row mb-8 fv-plugins-icon-container">
            <div class="fv-plugins-message-container invalid-feedback"></div></div>
            <div class="fv-row mb-8 fv-plugins-icon-container">
            <div class="fv-plugins-message-container invalid-feedback"></div></div>
            <!--end::Accept-->
            <!--begin::Submit button-->
            <div class="d-grid mb-10">
              <button type="submit"  class="btn btn-primary">Login              </button>
            </div>
            <div class="text-gray-500 text-center fw-semibold fs-6">Belum Punya Akun? 
            <a href="/register" class="link-primary fw-semibold">Daftar</a></div>
            <!--end::Sign up-->
          </div>
        </div>
        
      </div>
    </form>
 

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/widgets.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    
  </body>
</html>

