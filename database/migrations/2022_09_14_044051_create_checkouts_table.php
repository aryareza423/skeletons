<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
            $table->id();
            $table->double('total_harga');
            $table->enum('status', ['Pesanan Baru', 'Sedang DiProses', 'Dalam Pengiriman', 'Telah Selesai', 'Batal'])->default('Pesanan Baru')->nullable();
            $table->string('bukti_pembayaran')->nullable();
            
            $table->foreignId('user_id')->references('id')->on('users')
            ->onDelete('restrict');

            $table->foreignId('bank_id')->references('id')->on('banks')
            ->onDelete('restrict');

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
    }
}
