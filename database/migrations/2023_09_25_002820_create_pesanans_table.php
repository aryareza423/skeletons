<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanans', function (Blueprint $table) {
            $table->id();
            $table->integer('kuantitas');
            $table->double('sub_total');


            $table->foreignId('checkout_id')->references('id')->on('checkouts')
            ->onDelete('cascade');

            $table->foreignId('produk_id')->references('id')->on('produks')
            ->onDelete('cascade');

            $table->foreignId('size')->references('id')->on('sizes')
            ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanans');
    }
}
