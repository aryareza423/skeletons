<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengirimanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function (Blueprint $table) {
            $table->id();
            $table->string('harga_ongkir');
            $table->string('nama');
            $table->string('no_hp');
            $table->string('nm_kurir');
            $table->string('jenis_kurir');
            $table->string('nm_penerima');
            $table->string('alamat');
            $table->string('kode_pos');
            
            

            $table->foreignId('checkout_id')->references('id')->on('checkouts')
                  ->onDelete('restrict');

                  $table->foreignId('kota_id')->references('id')->on('kotas')
                  ->onDelete('restrict');
      
                  $table->foreignId('provinsi_id')->references('id')->on('provinsis')
                  ->onDelete('restrict');

                  $table->foreignId('bank_id')->references('id')->on('banks')
                  ->onDelete('restrict');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman');
    }
}
