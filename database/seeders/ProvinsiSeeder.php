<?php

namespace Database\Seeders;


use App\Models\Provinsi;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;


class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provinsi::create([
            'nm_provinsi' => 'Bali',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Bangka Belitung',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Banten',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Bengkulu',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'DI Yogyakarta',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'DKI Jakarta',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Gorontalo',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Jambi',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Jawa Barat',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Jawa Tengah',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Jawa Timur',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kalimantan Barat',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kalimantan Selatan',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kalimantan Tengah',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kalimantan Timur',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kalimantan Utara',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Kepulauan Riau',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Lampung',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Maluku',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Maluku Utara',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Nanggroe Aceh Darussalam (NAD)',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Nusa Tenggara Barat (NTB)',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Nusa Tenggara Timur (NTT)',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Papua',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Papua Barat',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Riau',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sulawesi Barat',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sulawesi Selatan',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sulawesi Tengah',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sulawesi Tenggara',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sulawesi Utara',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sumatera Barat',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sumatera Selatan',
        ]);

        Provinsi::create([
            'nm_provinsi' => 'Sumatera Utara',
        ]);
    }
}
